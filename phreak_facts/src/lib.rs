//! Support package for phreak_engine.
//!
//! The phreak_facts module is used to separate the fact generation logic from the phreak_engine.
//!
#[macro_use]
extern crate serde;

use std::borrow::Borrow;
use std::cmp::Ordering;
use std::fmt::Debug;
use std::hash::{Hash, Hasher};

pub use generational_arena::{Arena, Index};
use serde::export::fmt::Error;
use serde::export::Formatter;

pub use data::Value;

mod data;

/// A FactAttribute consists of three values
///
/// The normal usage would be:
///  attribute.0 identifies the type of the attribute
///  attribute.1 identifies the value
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Hash)]
pub struct FactAttribute(pub Value, pub Value);

impl PartialOrd for FactAttribute {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let mut r = self.0.partial_cmp(&other.0);
        if r == Some(Ordering::Equal) {
            r = self.1.partial_cmp(&other.1);
        }
        r
    }
}

impl Ord for FactAttribute {
    fn cmp(&self, other: &Self) -> Ordering {
        let mut r = self.0.cmp(&other.0);
        if Ordering::Equal == r {
            r = self.1.cmp(&other.1);
        }
        r
    }
}

impl Debug for FactAttribute {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "<{}, {}>", &self.0, &self.1)
    }
}

/// FactObject contains grouped information around a single object.
///
/// Example: A Car can have multiple pieces of information associated:
///  The color is red.
///  It has 4 wheels, and 3 doors.
///  The brand is Ferrari
///
/// This information is stored in a standardized way for the phreak algorithm, in order to
/// efficiently match conditions against the known facts.
#[derive(Serialize, Deserialize)]
pub struct FactObject {
    class: String,
    attrs: Vec<FactAttribute>,
}

impl FactObject {
    pub fn get_name(&self) -> &str {
        &self.class
    }

    pub fn get_attribute(&self, name: &str) -> Option<&FactAttribute> {
        self.attrs.iter()
            .find(|&attr| attr.0 == Value::from(name))

    }
    pub fn iter_attrs(&self) -> std::slice::Iter<'_, FactAttribute> {
        self.attrs.iter()
    }
}

impl Debug for FactObject {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let v: Vec<(&Value, &Value)> = self.attrs.iter().map(|attr| {
            (&attr.0, &attr.1)
        }).collect();
        write!(f, "Facts{{ class='{}', attrs={:?} }}", self.class, v)
    }
}

impl Clone for FactObject {
    fn clone(&self) -> Self {
        FactObject { class: self.class.to_owned(), attrs: self.attrs.clone() }
    }
}

impl Hash for FactObject {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.class.hash(state);
        for attr in self.attrs.iter() {
            let b: &FactAttribute = attr.borrow();
            b.0.hash(state);
            b.1.hash(state);
        }
    }
}

impl PartialEq for FactObject {
    fn eq(&self, other: &Self) -> bool {
        let mut b = self.class == other.class && self.attrs.len() == other.attrs.len();
        let mut i = 0;
        while b && i < self.attrs.len() {
            let left: &FactAttribute = self.attrs[i].borrow();
            let right: &FactAttribute = other.attrs[i].borrow();
            b = left.0 == right.0 && left.1 == right.1;
            i += 1;
        }
        b
    }
}

/// To facilitate the creation of facts, we offer a builder pattern.
///
/// To simplify the conversion from an object into a facts structure, we offer this
/// builder pattern.
///
/// ```
/// # use phreak_facts::FactsBuilder;
/// let facts = FactsBuilder::new("Car".to_owned())
///     .add_attributes("color".to_string(), "red".to_string())
///     .add_attributes("type".to_string(), "hatchback".to_string())
///     .add_attributes("brand".to_string(), "Toyota".to_string())
///     .build()
/// ;
///
/// ```
pub struct FactsBuilder {
    f: Option<FactObject>,
}

impl FactsBuilder {
    /// Start building a new Facts object with the given class name
    pub fn new(name: String) -> Self {
        FactsBuilder { f: Some(FactObject { class: name, attrs: vec!() }) }
    }

    /// Add one triple of information
    ///
    /// Triples can be of the format ( instance-id, attribute, value )
    pub fn add_attributes(mut self, field1: String, field2: String) -> Self {
        match &mut self.f {
            None => panic!("Set classname first"),
            Some(fact) => {
                fact.attrs.push(FactAttribute(
                    Value::from(field1),
                    Value::from(field2),
                ));
            }
        }
        self
    }

    /// Add one triple of information
    ///
    /// Triples can be of the format ( instance-id, attribute, value )
    pub fn add_attributes_from_slice(mut self, field1: &str, field2: &str) -> Self {
        match &mut self.f {
            None => panic!("Set classname first"),
            Some(fact) => {
                let f = FactAttribute(
                    Value::from(field1),
                    Value::from(field2),
                );
                fact.attrs.push(f);
            }
        }
        self
    }

    pub fn add_values(mut self, field1: Value, field2: Value) -> Self {
        match &mut self.f {
            None => panic!("Set classname first"),
            Some(fact) => {
                fact.attrs.push(FactAttribute(field1, field2));
            }
        }
        self
    }

    /// Finalize the facts object and return it
    pub fn build(self) -> FactObject {
        self.f.unwrap()
    }
}


#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case("1", "2")]
    fn test_facts_store(f1: &str, f2: &str) {
        let _fact = FactsBuilder::new("class1".to_owned())
            .add_attributes_from_slice(f1, f2)
            .build();

        // let mut s = Store::new();
        // let index = s.add_fact(fact);
        //
        let _fact = FactsBuilder::new("class1".to_owned())
            .add_attributes_from_slice(f1, f2)
            .build();
        // assert_eq!(s.find_fact(&fact), Some(index));
        //
        // s.remove_fact(index);
        // assert_eq!(s.find_fact(&fact), None);
    }
}
