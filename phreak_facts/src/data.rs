use std::cmp::Ordering;
use std::fmt::{Debug, Display};
use std::hash::Hash;

use chrono::prelude::*;
pub use generational_arena::{Arena, Index};
use serde::export::fmt::Error;
use serde::export::Formatter;
use smallstr::SmallString;
use uuid::Uuid;

/// Wrapper for values of different types
#[derive(Clone, Serialize, Deserialize, Hash, PartialEq)]
pub enum Value {
    None,
    Integer(i64),
    Uuid(Uuid),
    DateTime(chrono::DateTime<Utc>),
    String(SmallString<[u8; SMALL_STRING_SIZE]>),
    Object(Index), // hard dependency on the implementation type for fact handles in phreak_engine
}

impl Value {
    /// Create a Value containing the empty string
    pub fn new() -> Self { Self::default() }

    /// Returns the type-name of the Value
    /// ```
    /// # use phreak_facts::Value;
    ///
    /// assert_eq!(Value::from("text").describe(), "string");
    /// assert_eq!(Value::from(42).describe(), "integer");
    /// ```
    pub fn describe(&self) -> &str {
        match self {
            Value::None => { "none" }
            Value::String(_) => "string",
            Value::Uuid(_) => "uuid",
            Value::Integer(_) => "integer",
            Value::DateTime(_) => "datetime",
            Value::Object(_) => "object"
        }
    }

    /// Compare function that doesn't convert types, but only compares
    /// compatible types, resulting in a Partial Ordening
    ///
    /// The standard ordening of Value, as implemented by PartialOrd and Ord
    /// performs ordering based on type-order as to meet the strict requirements imposed
    /// by Hash and BTree datatypes. This is not the desired behavior for
    /// comparing in the Phreak network, so we provide a type-aware alternative.
    ///
    /// for PartialOrd the following holds Int.cmp(Str) == Some(Ordering::Less), or
    /// Integer < String, while Int.typed_cmp(Str) == None, so no ordering between types.
    ///
    /// ```
    /// # use phreak_facts::Value;
    /// # use std::cmp::Ordering;
    /// # use uuid::Uuid;
    ///
    /// // compare different numeric types
    /// assert_eq!(Value::from(15u8).typed_cmp( &Value::from(15i32) ), Some(Ordering::Equal));
    /// assert_eq!(Value::from(15u8).typed_cmp( &Value::from(5i32) ), Some(Ordering::Greater));
    /// assert_eq!(Value::from(8u8).typed_cmp( &Value::from(15i32) ), Some(Ordering::Less));
    ///
    /// let t = String::from("text");
    ///
    /// // compare &str and String
    /// assert_eq!(Value::from("text").typed_cmp( &Value::from(t) ), Some(Ordering::Equal));
    /// assert_eq!(Value::from("b").typed_cmp( &Value::from("a") ), Some(Ordering::Greater));
    ///
    /// // can't compare strings and integers
    /// assert_eq!(Value::from("5").typed_cmp( &Value::from(5u8) ), None);
    /// // or string and uuid
    /// assert_eq!(Value::from("5").typed_cmp( &Value::from(Uuid::new_v4()) ), None);
    /// // or uuid and integers
    /// assert_eq!(Value::from(Uuid::new_v4()).typed_cmp( &Value::from(5u8) ), None);
    ///
    /// // whereas partial_cmp and cmp succeed (strings come after integers in our storage)
    /// assert_eq!(Value::from("5").partial_cmp( &Value::from(5u8) ), Some(Ordering::Greater));
    /// assert_eq!(Value::from("5").cmp( &Value::from(5u8) ), Ordering::Greater);
    /// ```
    pub fn typed_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Value::None => {
                match other {
                    Value::None => Some(Ordering::Equal),
                    _ => None,
                }
            }
            Value::String(s) => {
                match other {
                    Value::String(o) => { Some(s.cmp(&o)) }
                    _ => { None }
                }
            }
            Value::Integer(s) => {
                match other {
                    Value::Integer(o) => { Some(s.cmp(&o)) }
                    _ => { None }
                }
            }
            Value::Uuid(s) => {
                match other {
                    Value::Uuid(o) => { Some(s.cmp(&o)) }
                    _ => { None }
                }
            }
            Value::DateTime(s) => {
                match other {
                    Value::DateTime(o) => { Some(s.cmp(&o)) }
                    _ => { None }
                }
            }
            Value::Object(s) => {
                match other {
                    Value::Object(o) => { Some(s.cmp(&o)) }
                    _ => { None }
                }
            }
        }
    }

    /// Equality function that doesn't convert types, but checks the type of the value.
    ///
    /// The standard equality of Value, as implemented by PartialEq and Eq
    /// does type conversions in order to meet the strict requirements imposed
    /// by Hash and BTree datatypes. This is not the desired behavior for
    /// equality in the Phreak network, so we provide a type-aware alternative.
    ///
    /// ```
    /// # use phreak_facts::Value;
    ///
    /// // compare different numeric types
    /// assert_eq!(Value::from(15u8).typed_eq( &Value::from(15i32) ), true);
    /// assert_eq!(Value::from(15u8).typed_eq( &Value::from(5i32) ), false);
    ///
    /// // compare int and &str
    /// assert_eq!(Value::from(8).typed_eq( &Value::from("8") ), false);
    ///
    /// // compare &str and String
    /// assert_eq!(Value::from("8").typed_eq( &Value::from("8".to_string()) ), true);
    /// ```
    pub fn typed_eq(&self, other: &Self) -> bool {
        match self {
            Value::None => {
                match other {
                    Value::None => true,
                    _ => false,
                }
            }
            Value::String(s) => {
                match other {
                    Value::String(o) => { s == o }
                    _ => { false }
                }
            }
            Value::Integer(s) => {
                match other {
                    Value::Integer(o) => { s == o }
                    _ => { false }
                }
            }
            Value::Uuid(s) => {
                match other {
                    Value::Uuid(o) => { s == o }
                    _ => { false }
                }
            }
            Value::DateTime(s) => {
                match other {
                    Value::DateTime(o) => { s == o }
                    _ => { false }
                }
            }
            Value::Object(s) => {
                match other {
                    Value::Object(o) => { s == o }
                    _ => { false }
                }
            }
        }
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            Value::None => { write!(f, "none") }
            Value::String(s) => { write!(f, "{}", &s) }
            Value::Integer(i) => { write!(f, "{}", &i) }
            Value::Uuid(u) => { write!(f, "{}", &u) }
            Value::DateTime(u) => { write!(f, "{}", &u) }
            Value::Object(o) => { write!(f, "{:?}", &o) }
        }
    }
}

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            Value::None => { write!(f, "None") }
            Value::String(s) => { write!(f, "String<\"{}\">", &s) }
            Value::Integer(i) => { write!(f, "Integer<{}>", &i) }
            Value::Uuid(u) => { write!(f, "Uuid<{}>", &u) }
            Value::DateTime(u) => { write!(f, "DateTime<{}>", &u) }
            Value::Object(o) => { write!(f, "Object<{:?}>", &o) }
        }
    }
}

impl Default for Value {
    fn default() -> Self {
        Value::None
    }
}

impl Eq for Value {}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Value::None => {
                match other {
                    Value::None => Some(Ordering::Equal),
                    _ => Some(Ordering::Greater),
                }
            }
            Value::String(s) => {
                match other {
                    Value::String(o) => s.partial_cmp(o),
                    Value::Object(_) => Some(Ordering::Less),
                    Value::None => Some(Ordering::Less),
                    _ => Some(Ordering::Greater),
                }
            }
            Value::Integer(s) => {
                match other {
                    Value::Integer(o) => { s.partial_cmp(o) }
                    _ => Some(Ordering::Less),
                }
            }
            Value::Uuid(s) => {
                match other {
                    Value::Integer(_) => Some(Ordering::Greater),
                    Value::Uuid(o) => { s.partial_cmp(o) }
                    Value::DateTime(_) => Some(Ordering::Less),
                    Value::String(_) => Some(Ordering::Less),
                    Value::Object(_) => Some(Ordering::Less),
                    Value::None => Some(Ordering::Less),
                }
            }
            Value::DateTime(s) => {
                match other {
                    Value::Integer(_) => Some(Ordering::Greater),
                    Value::Uuid(_) => Some(Ordering::Greater),
                    Value::DateTime(o) => { s.partial_cmp(o) }
                    Value::String(_) => Some(Ordering::Less),
                    Value::Object(_) => Some(Ordering::Less),
                    Value::None => Some(Ordering::Less),
                }
            }
            Value::Object(s) => {
                match other {
                    Value::Integer(_) => Some(Ordering::Greater),
                    Value::Uuid(_) => Some(Ordering::Greater),
                    Value::DateTime(_) => Some(Ordering::Greater),
                    Value::String(_) => Some(Ordering::Greater),
                    Value::Object(o) => { s.partial_cmp(o) }
                    Value::None => Some(Ordering::Less),
                }
            }
        }
    }
}

impl Ord for Value {
    fn cmp(&self, other: &Self) -> Ordering {
        match self {
            Value::None => {
                match other {
                    Value::None => Ordering::Equal,
                    _ => Ordering::Greater,
                }
            }
            Value::String(s) => {
                match other {
                    Value::String(o) => { s.cmp(o) }
                    Value::Object(_) => Ordering::Less,
                    Value::None => Ordering::Less,
                    _ => Ordering::Greater,
                }
            }
            Value::Integer(s) => {
                match other {
                    Value::Integer(o) => { s.cmp(o) }
                    Value::Object(_) => Ordering::Less,
                    Value::None => Ordering::Less,
                    _ => Ordering::Less,
                }
            }
            Value::Uuid(s) => {
                match other {
                    Value::Integer(_) => Ordering::Greater,
                    Value::Uuid(o) => { s.cmp(o) }
                    Value::DateTime(_) => Ordering::Less,
                    Value::String(_) => Ordering::Less,
                    Value::Object(_) => Ordering::Less,
                    Value::None => Ordering::Less,
                }
            }
            Value::DateTime(s) => {
                match other {
                    Value::Integer(_) => Ordering::Greater,
                    Value::Uuid(_) => Ordering::Greater,
                    Value::DateTime(o) => { s.cmp(o) }
                    Value::String(_) => Ordering::Less,
                    Value::Object(_) => Ordering::Less,
                    Value::None => Ordering::Less,
                }
            }
            Value::Object(s) => {
                match other {
                    Value::Object(o) => { s.cmp(o) }
                    Value::None => Ordering::Less,
                    _ => Ordering::Greater,
                }
            }
        }
    }
}

impl From<String> for Value {
    fn from(s: String) -> Self {
        Value::String(From::from(s))
    }
}

impl From<&String> for Value {
    fn from(s: &String) -> Self {
        Value::String(From::from(s.as_str()))
    }
}

impl From<&str> for Value {
    fn from(s: &str) -> Self {
        Value::String(From::from(s))
    }
}

impl From<&SmallString<[u8; SMALL_STRING_SIZE]>> for Value {
    fn from(s: &SmallString<[u8; SMALL_STRING_SIZE]>) -> Self {
        Value::String(s.clone())
    }
}

impl From<SmallString<[u8; SMALL_STRING_SIZE]>> for Value {
    fn from(s: SmallString<[u8; SMALL_STRING_SIZE]>) -> Self {
        Value::String(s)
    }
}

impl From<i64> for Value {
    fn from(v: i64) -> Self {
        Value::Integer(v)
    }
}

impl From<i32> for Value {
    fn from(v: i32) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<i16> for Value {
    fn from(v: i16) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<i8> for Value {
    fn from(v: i8) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<u32> for Value {
    fn from(v: u32) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<u16> for Value {
    fn from(v: u16) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<u8> for Value {
    fn from(v: u8) -> Self {
        Value::Integer(v as i64)
    }
}

impl From<Uuid> for Value {
    fn from(v: Uuid) -> Self {
        Value::Uuid(v)
    }
}

impl From<Index> for Value {
    fn from(v: Index) -> Self {
        Value::Object(v)
    }
}

impl From<&Index> for Value {
    fn from(v: &Index) -> Self {
        Value::Object(*v)
    }
}

impl<T: chrono::offset::TimeZone> From<DateTime<T>> for Value {
    fn from(v: DateTime<T>) -> Self {
        Value::DateTime(v.with_timezone(&Utc))
    }
}

/// Break-off point at which storing inline strings switches to allocating on heap
pub(crate) const SMALL_STRING_SIZE: usize = 10;

#[cfg(test)]
mod tests {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::Hasher;

    use test_case::test_case;

    use super::*;

    fn hash(v: &Value) -> u64 {
        let mut hasher = DefaultHasher::new();
        v.hash(&mut hasher);
        hasher.finish()
    }

    // test two Value's for equality and hash value
    // important that eq => hash, so the result (true, false) is forbidden
    #[test_case(Value::from("one"), Value::from("one") => (true, true); "test equal strings")]
    #[test_case(Value::from(1), Value::from("1") => (false, false); "test equal int and string")]
    #[test_case(Value::from("1"), Value::from(1) => (false, false); "test equal string and int")]
    #[test_case(Value::from("one"), Value::from("two") => (false, false); "test different strings")]
    #[test_case(Value::from(1), Value::from(2) => (false, false); "test different ints")]
    fn test_eq(v1: Value, v2: Value) -> (bool, bool) {
        (v1 == v2, hash(&v1) == hash(&v2))
    }

    #[test_case(Value::from("test") => Value::String(From::from("test")))]
    #[test_case(Value::from("test".to_string()) => Value::String(From::from("test")))]
    #[test_case(Value::from(3u8) => Value::Integer(3))]
    #[test_case(Value::from(3u16) => Value::Integer(3))]
    #[test_case(Value::from(3u32) => Value::Integer(3))]
    #[test_case(Value::from(3i8) => Value::Integer(3))]
    #[test_case(Value::from(3i16) => Value::Integer(3))]
    #[test_case(Value::from(3i32) => Value::Integer(3))]
    #[test_case(Value::from(3i64) => Value::Integer(3))]
    fn test_from(v: Value) -> Value { v }

    // Ordering is used in data structures to sort the values. It must be
    // consistent with equality and hashing, and must be a total order
    #[test_case(Value::from(3i64), Value::from(3) => Ordering::Equal)]
    #[test_case(Value::from(3), Value::from("3") => Ordering::Less)]
    #[test_case(Value::from(2), Value::from("3") => Ordering::Less)]
    #[test_case(Value::from(20), Value::from("3") => Ordering::Less; "lexical ordering")]
    #[test_case(Value::from(2), Value::from(1) => Ordering::Greater)]
    fn test_ord(a: Value, b: Value) -> Ordering {
        let r = a.cmp(&b);
        match r {
            Ordering::Equal => {
                assert!(a.eq(&b));
                assert_eq!(hash(&a), hash(&b));
            }
            _ => {
                assert!(!a.eq(&b));
                assert_ne!(hash(&a), hash(&b));
            }
        }
        assert_eq!(a.partial_cmp(&b), Some(r));
        r
    }

    #[test]
    fn test_ordering_types() {
        let s = Value::from("1");
        let i = Value::from(1);
        let u = Value::from(Uuid::new_v4());
        let d = Value::from(DateTime::parse_from_rfc2822("Wed, 18 Feb 2015 23:16:09 GMT").unwrap());

        // i < u < d < s < o
        assert_eq!(i.cmp(&u), Ordering::Less);
        assert_eq!(i.cmp(&d), Ordering::Less);
        assert_eq!(i.cmp(&s), Ordering::Less);
        assert_eq!(u.cmp(&d), Ordering::Less);
        assert_eq!(u.cmp(&s), Ordering::Less);
        assert_eq!(d.cmp(&s), Ordering::Less);
        assert_eq!(s.cmp(&i), Ordering::Greater);
        assert_eq!(s.cmp(&u), Ordering::Greater);
        assert_eq!(s.cmp(&d), Ordering::Greater);
        assert_eq!(d.cmp(&i), Ordering::Greater);
        assert_eq!(d.cmp(&u), Ordering::Greater);
        assert_eq!(u.cmp(&i), Ordering::Greater);
    }
}
