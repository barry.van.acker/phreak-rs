//! The parser package contains the logic to create rules for the Phreak algorithm.
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
