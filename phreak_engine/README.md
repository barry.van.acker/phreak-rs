# phreak_engine

[![License GPL3+](https://img.shields.io/crates/l/phreak_engine.svg)](https://gitlab.com/barry.van.acker/phreak-rs/-/blob/develop/phreak_engine/LICENSE.md)
[![Crates.io](https://img.shields.io/crates/v/phreak_engine.svg)](https://crates.io/crates/phreak_engine)
[![doc.rs](https://docs.rs/phreak_engine/badge.svg)](https://docs.rs/phreak_engine)
[![dependency status](https://deps.rs/repo/gitlab/barry.van.acker/phreak-rs/status.svg)](https://deps.rs/repo/gitlab/barry.van.acker/phreak-rs)

The Phreak_engine package contains a rules engine, an optimized fact matching program, 
using the phreak algorithm.

Fact matching algorithms are used as implementations underneath many vendor products for
Complex Event Processing CEP, Decision Engines, Planners and BPM tools.

# WORK IN PROGRESS
This is the beginning of a concept, and definitely not stable in any way. It is a study of the phreak
algorithm that might be usefull some day. 