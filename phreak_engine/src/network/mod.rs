//! The network module contains the structure corresponding to the compiled rules of
//! a ruleset. It is independent of the facts that are fed to the system at runtime.
//!
//! Rules can be compiled and added to a running engine dynamically, but they remain
//! static otherwise. No changes happen here when data is processed, only when rules
//! are modified.

use generational_arena::{Arena, Index as ArenaIndex};
use generational_indextree::{Arena as TreeArena, NodeId as TreeIndex};
use serde::{Deserialize, Serialize};

use crate::config::*;
use crate::network::nodes::{PhreakAlphaNode, PhreakBetaNode};
use crate::network::partitions::Partition;
use crate::network::paths::Path;
use crate::network::segments::Segment;

pub mod nodes;
pub mod segments;
pub mod paths;
pub mod partitions;
pub mod conditions;
pub mod builder;

pub mod object_propagation;

/// Identifier for Phreak Alpha Nodes
pub type PhreakAlphaNodeId = TreeIndex;

/// Identifier for Phreak Beta Nodes
pub type PhreakBetaNodeId = TreeIndex;

/// Identifier for Segments
pub type SegmentId = ArenaIndex;

/// Identifier for Paths
pub type PathId = ArenaIndex;

/// Identifier for Partitions
pub type PartitionId = ArenaIndex;

/// The Network contains the structure of the rule matching algorithm. This is considered to be
/// the 'static' part of the data structure, as it only changes when rules are modified.
#[derive(Deserialize, Serialize)]
pub struct Network {
    pub(crate) alphanodes: TreeArena<PhreakAlphaNode>,
    pub(crate) betanodes: TreeArena<PhreakBetaNode>,
    pub(crate) segments: Arena<Segment>,
    pub(crate) paths: Arena<Path>,
    pub partitions: Arena<Partition>,
}

impl Network {
    pub fn new() -> Self {
        Network {
            alphanodes: TreeArena::with_capacity(START_ALPHANETWORK_SIZE),
            betanodes: TreeArena::with_capacity(START_BETANETWORK_SIZE),
            segments: Arena::with_capacity(START_SEGMENTS_SIZE),
            paths: Arena::with_capacity(START_PATHS_SIZE),
            partitions: Arena::with_capacity(START_PARTITIONS_SIZE),
        }
    }
}

impl Default for Network {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests;
