use std::sync::Mutex;

use crate::config::GeneralConfig;
use crate::memory::{KnowledgeSession, PartitionMemoryId};
use crate::memory::partitionmemory::PartitionMemory;
use crate::memory::segmentmemory::SegmentMemory;
use crate::network::*;
use crate::network::builder::memory_builder::create_beta_memory;
use crate::network::nodes::{PhreakAlphaNode, PhreakBetaNode, PhreakBetaNodeType};
use crate::network::nodes::joinnode::JoinNode;
use crate::network::nodes::lianode::LIANode;
use crate::network::nodes::rightbridgenode::RightBridgeNode;
use crate::network::partitions::Partition;
use crate::network::paths::{Path, TipNode};
use crate::network::segments::Segment;
use crate::network::conditions::JoinConstraint;

pub struct BetaNetworkBuilder<'a> {
    network: &'a mut Network,
    ks: &'a mut KnowledgeSession,
    config: &'a GeneralConfig,
    last_betanode: Option<PhreakBetaNodeId>,
    entrypoint: Option<PhreakBetaNodeId>,
    current_segment: Option<SegmentId>,
    current_path: Option<PathId>,
    current_partition: Option<PartitionId>,
    lia_node: Option<PhreakAlphaNodeId>,

}

impl<'a> BetaNetworkBuilder<'a> {
    /// Create a new rule builder for this network
    pub fn new(network: &'a mut Network, ks: &'a mut KnowledgeSession, config: &'a GeneralConfig) -> Self {
        BetaNetworkBuilder {
            network,
            ks,
            config,
            last_betanode: None,
            entrypoint: None,
            current_segment: None,
            current_path: None,
            current_partition: None,
            lia_node: None,
        }
    }

    /// Finalize the network and return the starting and ending points
    pub fn collect(self, start_id: PhreakAlphaNodeId, tip_id: PhreakAlphaNodeId) -> PartitionId
    {
        let pid = self.current_partition.unwrap();

        self.network.paths[self.current_path.unwrap()].tip_node =
            match self.last_betanode {
                None => TipNode::Alpha(tip_id),
                Some(tip_id) => TipNode::Beta(tip_id),
            };

        // build segment list for path
        {
            let mut s = vec!();
            let mut current_node = self.last_betanode;
            let mut last_segment = None;

            while let Some(node) = current_node {
                let n = &self.network.betanodes[node];
                let sid = n.get().get_segment_id();
                if Some(sid) != last_segment {
                    s.push(sid);
                    last_segment = Some(sid);
                }

                current_node = n.parent();
            }

            s.reverse();
            println!("path: {:?}", s);
            self.network.paths[self.current_path.unwrap()].segments = s;
        }

        self
            .network
            .partitions[pid]
            .recalculate_entrynodes(&self.network.paths);

        pid
    }

    pub fn get_partition(&self) -> PartitionId { self.current_partition.unwrap() }

    /// Start a new partition for rule matching. Partitions can be processed
    /// in parallel, but rules can't share intermediate state between partitions.
    pub fn start_partition(mut self, pm_id: &mut Option<PartitionMemoryId>) -> Self {
        pm_id.replace(self.ks
            .partition_memories
            .insert(
                Mutex::new(PartitionMemory::new())
            )
        );

        self.current_partition = Some(
            self
                .network
                .partitions
                .insert(
                    Partition::new(pm_id.unwrap())
                ));

        self
    }

    pub fn start_path(mut self, start_id: PhreakAlphaNodeId) -> Self {
        let pid = self.current_partition.unwrap();

        self.current_path = Some(self.network.paths.insert(
            Path::new(start_id)
        ));

        self.network.partitions[pid].paths.push(self.current_path.unwrap());
        self
    }

    pub fn start_segment(mut self, pm_id: PartitionMemoryId) -> Self {
        let pid = self.current_partition.unwrap();
        let partition = &mut self.network.partitions[pid];


        {
            let partition_memory = &mut self.ks
                .partition_memories[pm_id].lock().unwrap();

            let memory_id = partition_memory
                .segment_memories
                .insert(
                    SegmentMemory::new()
                );

            self.current_segment = Some(self.network.segments.insert(
                Segment::new(memory_id)
            ));
        }
        self
    }

    /// Technically an Alpha node, but we handle bridging nodes here so that each builder only
    /// deals with a linear structure.
    pub fn create_lia_node(mut self, objecttype_node_id: PhreakAlphaNodeId, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let lian_id = self.network.alphanodes.new_node(PhreakAlphaNode::LIANode(
            LIANode::default()
        ));
        id.replace(lian_id);

        self.lia_node = Some(lian_id);
        objecttype_node_id.append(lian_id, &mut self.network.alphanodes);

        self
    }

    /// Technically an Alpha node, but we handle bridging nodes here so that each builder only
    /// deals with a linear structure.
    pub fn get_or_create_lia_node(mut self, objecttype_node_id: PhreakAlphaNodeId, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let lian = objecttype_node_id
            .children(&self.network.alphanodes)
            .find(|&child_id| self.network.alphanodes[child_id].get().is_lianode());

        match lian {
            Some(lian_id) => {
                self.lia_node = Some(lian_id);
                self
            }
            None => {
                self.create_lia_node(objecttype_node_id, id)
            }
        }
    }

    pub fn create_betajoin_node(
        mut self,
        alpha_node_id: PhreakAlphaNodeId,
        id: &mut Option<PhreakBetaNodeId>,
        pm_id: PartitionMemoryId,
        constraint: JoinConstraint,
    )
        -> Self
    {
        debug_assert!(self.lia_node.is_some());

        let ks = &mut *self.ks;
        let network = &mut *self.network;
        let partition = &mut ks.partition_memories[pm_id];

        let rightbridge_node = alpha_node_id
            .children(&network.alphanodes)
            .find(|&child_id| network.alphanodes[child_id].get().is_rbnode());

        let rb_id = match rightbridge_node {
            Some(rb_id) => {
                rb_id
            }
            None => {
                let bid = network.alphanodes.new_node(
                    PhreakAlphaNode::RightBridgeNode(RightBridgeNode::default())
                );
                alpha_node_id.append(bid, &mut network.alphanodes);
                bid
            }
        };

        let mut constraints = vec!(constraint);
        let betamemory_id = partition.lock().unwrap()
            .beta_memories
            .insert(
                create_beta_memory(self.config, PhreakBetaNodeType::JoinNode, constraints.as_slice())
            );

        let sid = match self.last_betanode {
            Some(node_id) => { self.split_segment(node_id) }
            None => { self.current_segment.unwrap() }
        };

        let join_id = self.network.betanodes.new_node(
            PhreakBetaNode::JoinNode(JoinNode::new(
                betamemory_id,
                rb_id,
                sid,
                self.current_partition.unwrap(),
                constraints.pop().unwrap()
            ))
        );

        self.network.segments[sid].nodes.push(join_id);

        match self.network.alphanodes[rb_id].get_mut() {
            PhreakAlphaNode::RightBridgeNode(n) => {
                n.add_betanode(join_id);
            }
            _ => { unreachable!() }
        }

        match self.last_betanode {
            Some(parent_id) => {
                // last betanode becomes the parent when available
                parent_id.append(join_id, &mut self.network.betanodes);
            }
            None => {
                // when no parent is available, this will be the root (entrypoint) and
                // a LIANode will provide the initial left tuples
                match self.network.alphanodes[self.lia_node.unwrap()].get_mut() {
                    PhreakAlphaNode::LIANode(n) => {
                        n.add_betanode(join_id);
                    }
                    _ => { unreachable!() }
                }

                self.entrypoint = Some(join_id);
            }
        }

        self.last_betanode = Some(join_id);
        id.replace(join_id);
        self
    }

    pub fn get_or_create_betajoin_node(
        mut self,
        alpha_node_id: PhreakAlphaNodeId,
        id: &mut Option<PhreakBetaNodeId>,
        pm_id: PartitionMemoryId,
        constraint: JoinConstraint,
    ) -> Self {
        let child_id = self
            .last_betanode
            .and_then(|parent_id|
                // find the child with the correct alpha node link
                parent_id
                    .children(&self.network.betanodes)
                    .find(|&child_id|
                        match self.network.betanodes[child_id].get() {
                            PhreakBetaNode::JoinNode(n) => {
                                self.network.alphanodes[n.get_rightbridgenode_id()]
                                    .parent()
                                    .unwrap() == alpha_node_id
                                &&
                                    n.constraint == constraint
                            }
                            _ => { false }
                        })
            );

        match child_id {
            Some(bn_id) => {
                id.replace(bn_id);
                self.last_betanode = Some(bn_id);
                self
            }
            None => {
                self.create_betajoin_node(alpha_node_id, id, pm_id, constraint)
            }
        }
    }

    /// split the current segment after the given betanode id, and return
    /// a fresh segment id to be used by the new nodes inserted after the
    /// given betanode.
    fn split_segment(&mut self, after_id: PhreakBetaNodeId) -> SegmentId {
        let node = &self.network.betanodes[after_id];
        let childcount = after_id.children(&self.network.betanodes).count();
        let smems = &mut self.ks.partition_memories[node.get().get_partition_id()].lock().unwrap().segment_memories;

        if childcount == 0 { // no new segment required
            node.get().get_segment_id()
        } else if childcount == 1 { // split may be required, and create a new segment for the new split
            let segment_to_split = node.get().get_segment_id();

            let old_tail = self.network.segments.insert(
                Segment::new(smems.insert(SegmentMemory::new()))
            );

            let mut current = Some(after_id);
            while let Some(node) = current {
                self.network.betanodes[node].get_mut().set_segment_id(old_tail);
                current = self.network.betanodes[node].first_child();
            }

            self.network.segments.insert(
                Segment::new(smems.insert(SegmentMemory::new()))
            )
        } else { // no split required, just create a new segment
            self.network.segments.insert(
                Segment::new(smems.insert(SegmentMemory::new()))
            )
        }
    }
}