use std::hash::Hash;

use serde::{Deserialize, Serialize};
use smallstr::SmallString;

use phreak_facts::{FactObject, Value};

use crate::config::*;
use crate::memory::betamemory::BetaMemory;
use crate::memory::partitionmemory::{LeftTupleId, RightTupleId};
use crate::memory::tuplememory::{LeftTupleMemoryEnum, RightTupleMemoryEnum};
use crate::memory::tuplememory::tuple_hash_memory::{LeftTupleHashMemory, RightTupleHashMemory};
use crate::memory::tuplememory::tuple_list_memory::TupleListMemory;
use crate::memory::tuples::{LeftTuple, RightTuple, Tuple};
use crate::network::conditions::{ConstraintType, JoinConstraint};
use crate::network::nodes::PhreakBetaNodeType;
use crate::store::{FactHandle, Store};

pub(crate) fn create_beta_memory(config: &GeneralConfig, node_type: PhreakBetaNodeType, constraints: &[JoinConstraint]) -> BetaMemory {
    let key_depth = config.composite_key_depth;
    if key_depth < 1 {
        let ltm: LeftTupleMemoryEnum = match config.sequential {
            true => {
                LeftTupleMemoryEnum::None
            }
            false => {
                LeftTupleMemoryEnum::TupleList(TupleListMemory::new())
            }
        };

        let rtm: RightTupleMemoryEnum = RightTupleMemoryEnum::TupleList(TupleListMemory::new());

        BetaMemory::new(ltm, rtm, node_type)
    } else {
        let spec = IndexSpec::new(config.index_precedence_options, key_depth,
                                  node_type, constraints,
        );

        BetaMemory::new(
            create_left_memory(config, &spec),
            create_right_memory(config, &spec),
            node_type,
        )
    }
}

fn create_right_memory(config: &GeneralConfig, spec: &IndexSpec) -> RightTupleMemoryEnum {
    if !config.index_right_beta_memory || !spec.constraint_type.is_indexable()
    {
        return RightTupleMemoryEnum::TupleList(TupleListMemory::new());
    }

    if spec.constraint_type == ConstraintType::Equal
    {
        return RightTupleMemoryEnum::TupleHash(RightTupleHashMemory::new(spec));
    }

    // if spec.constraint_type.is_comparison()
    // {
    //     // RBTree
    // }

    // if spec.constraint_type == ConstraintType.RANGE
    // {
    //     // TreeMap ?
    // }

    RightTupleMemoryEnum::TupleList(TupleListMemory::new())
}

fn create_left_memory(config: &GeneralConfig, spec: &IndexSpec) -> LeftTupleMemoryEnum {
    if config.sequential {
        return LeftTupleMemoryEnum::None;
    }

    if !config.index_left_beta_memory || !spec.constraint_type.is_indexable()
    {
        return LeftTupleMemoryEnum::TupleList(TupleListMemory::new());
    }

    if spec.constraint_type == ConstraintType::Equal
    {
        return LeftTupleMemoryEnum::TupleHash(LeftTupleHashMemory::new(spec));
    }

    // if spec.constraint_type.is_comparison()
    // {
    //     // RBTree
    // }

    // if spec.constraint_type == ConstraintType.RANGE
    // {
    //     // TreeMap ?
    // }

    LeftTupleMemoryEnum::TupleList(TupleListMemory::new())
}

/// When creating indexes gives precedence to the equality constraints (default)
/// or to the first indexable constraint in the pattern.
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum IndexPrecedenceOption {
    EqualityPriority,
    PatternOrder,
}


/// A FieldIndexValue holds a value that can be hashed, so that we can quickly correlate two
/// FactObjects that match by (in)equality.
#[derive(Deserialize, Serialize, Hash, PartialEq, Eq, PartialOrd, Debug)]
pub enum FieldIndexValue {
    /// Hashing by identity is like comparing a pointer
    Identity(FactHandle),
    /// Hash by a value
    Field(Value),
}

/// A FieldIndex is a factory that knows how to extract FieldIndexValues from an object
#[derive(Clone, Deserialize, Serialize)]
pub enum FieldIndex {
    Identity,
    Field(SmallString<[u8; SMALL_STRING_SIZE]>),
}

impl FieldIndex
{
    /// Returns a factory that indexes by object identity, aka by FactHandle, as this is unique
    pub fn identity() -> Self { FieldIndex::Identity }

    /// Returns a factory that indexes on a single field
    pub fn field(name: &str) -> Self { FieldIndex::Field(From::from(name)) }

    /// Create an index for the given object
    pub fn make_index(&self, handle: FactHandle, obj: &FactObject) -> FieldIndexValue {
        match self {
            FieldIndex::Identity => {
                FieldIndexValue::Identity(handle)
            }
            FieldIndex::Field(name) => {
                match obj.get_attribute(name) {
                    Some(value) => FieldIndexValue::Field(From::from(value.1.to_string())),
                    None => FieldIndexValue::Field(Value::None),
                }
            }
        }
    }

    /// Create an index for the given object
    pub fn make_left_index(&self, tuple_id: LeftTupleId, t: &LeftTuple, s: &Store) -> FieldIndexValue {
        self.make_index(t.get_fact_handle(), &s[t.get_fact_handle()])
    }

    /// Create an index for the given object
    pub fn make_right_index(&self, tuple_id: RightTupleId, t: &RightTuple, s: &Store) -> FieldIndexValue {
        self.make_index(t.get_fact_handle(), &s[t.get_fact_handle()])
    }
}

/// IndexSpec describes an index configuration used to optimize joins.
/// This is used during memory creation, and while accessing the tuplememories.
#[derive(Clone, Deserialize, Serialize)]
pub struct IndexSpec {
    /// Different constraints allow for different optimisations
    constraint_type: ConstraintType,
    /// A maximum of four fields can be combined into an index
    indexes: [Option<FieldIndex>; 4],

    ascending_constraint_type: Option<ConstraintType>,

    descending_constraint_type: Option<ConstraintType>,
}

impl IndexSpec {
    pub fn new(precedence: IndexPrecedenceOption, key_depth: usize, node_type: PhreakBetaNodeType, constraints: &[JoinConstraint]) -> IndexSpec {
        let mut spec = IndexSpec {
            constraint_type: ConstraintType::Unknown,
            indexes: [None, None, None, None],
            ascending_constraint_type: None,
            descending_constraint_type: None,
        };

        let first_indexable_constraint = if precedence == IndexPrecedenceOption::EqualityPriority {
            spec.determine_type_with_equality_priority(node_type, constraints)
        } else {
            spec.determine_type_with_pattern_order(node_type, constraints)
        };

        if spec.constraint_type == ConstraintType::Equal {
            let mut index_list = vec!(constraints[first_indexable_constraint].get_field_index());

            for constraint in constraints.iter().skip(first_indexable_constraint + 1) {
                if key_depth >= index_list.len() {
                    break;
                }
                if constraint.get_constraint_type() == ConstraintType::Equal && !constraint.is_unification() {
                    index_list.push(constraint.get_field_index());
                }
            }

            spec.indexes
                .iter_mut()
                .zip(index_list)
                .for_each(|(w, r)| *w = Some(r))
            ;
        } else if spec.constraint_type.is_comparison() {
            // look for a dual constraint to create a range index
            /*
            if (USE_RANGE_INDEX && constraints[first_indexable_constraint] instanceof MvelConstraint) {

                MvelConstraint firstConstraint = (MvelConstraint) constraints[firstIndexableConstraint];
                String leftValue = getLeftValueInExpression(firstConstraint.getExpression());
                for (int i = firstIndexableConstraint+1; i < constraints.length; i++) {
                    if (constraints[i] instanceof MvelConstraint) {
                        MvelConstraint dualConstraint = (MvelConstraint) constraints[i];
                        if (isDual(firstConstraint, leftValue, dualConstraint)) {
                            constraintType = ConstraintType.RANGE;
                            if (firstConstraint.getConstraintType().isAscending()) {
                                ascendingConstraintType = firstConstraint.getConstraintType();
                                descendingConstraintType = dualConstraint.getConstraintType();
                                indexes = new FieldIndex[]{ firstConstraint.getFieldIndex(), dualConstraint.getFieldIndex() };
                            } else {
                                ascendingConstraintType = dualConstraint.getConstraintType();
                                descendingConstraintType = firstConstraint.getConstraintType();
                                indexes = new FieldIndex[]{ dualConstraint.getFieldIndex(), firstConstraint.getFieldIndex() };
                            }
                            return;
                        }
                    }
                }
            }
 */
            // indexes = new FieldIndex[]{ ((IndexableConstraint)constraints[firstIndexableConstraint]).getFieldIndex() };
            spec.indexes[0] = Some(constraints[first_indexable_constraint].get_field_index());
            todo!();
        }
        spec
    }

    fn determine_type_with_equality_priority(&mut self, node_type: PhreakBetaNodeType, constraints: &[JoinConstraint]) -> usize {
        let mut position = 0;

        for (i, constraint) in constraints.iter().enumerate() {
            if constraint.is_indexable() {
                let t = constraint.get_constraint_type();
                if t == ConstraintType::Equal {
                    self.constraint_type = ConstraintType::Equal;
                    return i;
                } else if t == ConstraintType::Unknown && t.is_indexable_for_node(node_type) {
                    self.constraint_type = t;
                    position = i;
                }
            }
        }

        position
    }

    fn determine_type_with_pattern_order(&mut self, node_type: PhreakBetaNodeType, constraints: &[JoinConstraint]) -> usize {
        for (i, constraint) in constraints.iter().enumerate() {
            let t = constraint.get_constraint_type();
            if t.is_indexable_for_node(node_type) {
                self.constraint_type = t;
                return i;
            }
        }

        constraints.len()
    }
}

/// The IndexKey is the concrete hashable result of applying an IndexSpec
///
/// We apply an IndexSpec to an Object to obtain upto four values that we can
/// Hash for hashmemories, or Order for ordered tree memories.
#[derive(Hash, PartialEq, Eq, PartialOrd, Debug, Deserialize, Serialize)]
pub struct IndexKey {
    values: [Option<FieldIndexValue>; 4],
}

impl IndexKey {
    pub fn new(spec: &IndexSpec, handle: FactHandle, obj: &FactObject) -> Self {
        IndexKey {
            values: [
                spec.indexes[0].as_ref()
                    .map(|field_index| field_index.make_index(handle, obj)),
                spec.indexes[1].as_ref()
                    .map(|field_index| field_index.make_index(handle, obj)),
                spec.indexes[2].as_ref()
                    .map(|field_index| field_index.make_index(handle, obj)),
                spec.indexes[3].as_ref()
                    .map(|field_index| field_index.make_index(handle, obj)),
            ]
        }
    }
}