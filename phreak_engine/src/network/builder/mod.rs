//! This module contains builder-helpers that allow us to create network structures.

pub mod alpha_builder;
pub mod beta_builder;
pub mod memory_builder;