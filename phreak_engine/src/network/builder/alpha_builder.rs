use crate::network::*;
use crate::network::conditions::Condition;
use crate::network::nodes::alphanode::AlphaNode;
use crate::network::nodes::objecttypenode::ObjectTypeNode;
use crate::network::nodes::PhreakAlphaNode;

pub struct AlphaNetworkBuilder<'a> {
    network: &'a mut Network,
    last_alphanode: Option<PhreakAlphaNodeId>,
    entrypoint: Option<PhreakAlphaNodeId>,
}

impl<'a> AlphaNetworkBuilder<'a> {
    /// Create a new rule builder for this network
    pub fn new(network: &'a mut Network) -> Self {
        AlphaNetworkBuilder { network, last_alphanode: None, entrypoint: None }
    }

    /// Finalize the network and return the starting and ending points
    pub fn collect(self) -> (PhreakAlphaNodeId, PhreakAlphaNodeId) {
        (self.entrypoint.unwrap(), self.last_alphanode.unwrap())
    }

    /// Create an entrypoint into the alpha network
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    ///
    /// let mut network = Network::new();
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .collect()
    /// ;
    /// ```
    pub fn create_entrypoint_node(mut self) -> Self {
        self.last_alphanode =
            Some(self
                .network
                .alphanodes
                .new_node(
                    PhreakAlphaNode::EntryNode(Default::default())
                ));
        self.entrypoint = self.last_alphanode;
        self
    }

    /// Set the entrypoint into the alpha network, and prepare the builder to
    /// continue building from this point.
    pub fn set_entrypoint_node(mut self, node_id: PhreakAlphaNodeId) -> Self {
        self.last_alphanode = Some(node_id);
        self.entrypoint = Some(node_id);
        self
    }

    /// Create an objecttype node in the alpha network
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    ///
    /// let mut network = Network::new();
    /// let mut on_id = None;
    ///
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .create_objecttype_node("Object", &mut on_id)
    ///     .collect()
    /// ;
    /// ```
    pub fn create_objecttype_node(mut self, object_name: &str, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let parent_id = self
            .last_alphanode
            .expect("ObjectTypeNode should come after an EntryNode");

        debug_assert!(self.network.alphanodes[parent_id].get().is_entrynode());

        let otn_id = self
            .network
            .alphanodes
            .new_node(
                PhreakAlphaNode::ObjectTypeNode(ObjectTypeNode::from(object_name))
            );
        parent_id.append(otn_id, &mut self.network.alphanodes);

        self.last_alphanode = Some(otn_id);
        id.replace(otn_id);
        self
    }

    /// Create an objecttype node in the alpha network
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    ///
    /// let mut network = Network::new();
    /// let mut on_id = None;
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .get_or_create_objecttype_node("Object", &mut on_id)
    ///     .collect()
    /// ;
    /// ```
    pub fn get_or_create_objecttype_node(mut self, object_name: &str, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let parent_id = self
            .last_alphanode
            .expect("ObjectTypeNode should come after an EntryNode");

        debug_assert!(self.network.alphanodes[parent_id].get().is_entrynode());

        let child_id = parent_id
            .children(&self.network.alphanodes)
            .find(|&id|
                match self
                    .network
                    .alphanodes[id]
                    .get() {
                    PhreakAlphaNode::ObjectTypeNode(otn) => {
                        otn.get_object_name() == object_name
                    }
                    _ => false
                });

        match child_id {
            Some(otn_id) => {
                id.replace(otn_id);
                self.last_alphanode = Some(otn_id);
                self
            }
            None => {
                self.create_objecttype_node(object_name, id)
            }
        }
    }

    /// Create an alpha node in the alpha network
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    /// # use phreak_engine::network::conditions::{Condition, Operator};
    /// # use phreak_facts::Value;
    ///
    /// let mut network = Network::new();
    /// let mut on_id = None;
    /// let mut an_id = None;
    ///
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .get_or_create_objecttype_node("Object", &mut on_id)
    ///     .create_alpha_node(Condition{
    ///         field: Value::from("color"),
    ///         operator: Operator::Equal,
    ///         expr: Value::from("red")
    ///     }, &mut an_id)
    ///     .collect()
    /// ;
    /// ```
    pub fn create_alpha_node(mut self, condition: Condition, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let parent_id = self
            .last_alphanode
            .expect("AlphaNode should come after an ObjectTypeNode");

        debug_assert!(self.network.alphanodes[parent_id].get().is_objecttypenode());

        let an_id = self
            .network
            .alphanodes
            .new_node(
                PhreakAlphaNode::AlphaNode(AlphaNode::new(condition))
            );
        parent_id.append(an_id, &mut self.network.alphanodes);

        self.last_alphanode = Some(an_id);
        id.replace(an_id);
        self
    }

    /// Create an alpha node in the alpha network
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    /// # use phreak_engine::network::conditions::{Condition, Operator};
    /// # use phreak_facts::Value;
    ///
    /// let mut network = Network::new();
    /// let mut on_id = None;
    /// let mut an_id = None;
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .get_or_create_objecttype_node("Object", &mut on_id)
    ///     .get_or_create_alpha_node(Condition{
    ///         field: Value::from("color"),
    ///         operator: Operator::Equal,
    ///         expr: Value::from("red")
    ///     }, &mut an_id)
    ///     .create_alphatermination_node()
    ///     .collect()
    /// ;
    /// ```
    pub fn get_or_create_alpha_node(mut self, condition: Condition, id: &mut Option<PhreakAlphaNodeId>) -> Self {
        let parent_id = self
            .last_alphanode
            .expect("AlphaNode should come after an ObjectTypeNode");

        debug_assert!(self.network.alphanodes[parent_id].get().is_objecttypenode());

        // find the parent's child matching the given condition
        let child = parent_id
            .children(&self.network.alphanodes)
            .find(|&node_id|
                match self.network.alphanodes[node_id].get() {
                    PhreakAlphaNode::AlphaNode(n) => {
                        *n.get_constraint() == condition
                    }
                    _ => false
                });

        // if child exists, reuse it, otherwise create a fresh one
        match child {
            Some(an_id) => {
                id.replace(an_id);
                self.last_alphanode = Some(an_id);
                self
            }
            None => {
                self.create_alpha_node(condition, id)
            }
        }
    }


    /// Create an alpha termination node in the alpha network
    ///
    /// This type of node can never be re-used.
    ///
    /// ```
    /// # use phreak_engine::network::Network;
    /// # use phreak_engine::network::builder::alpha_builder::AlphaNetworkBuilder;
    ///
    /// let mut network = Network::new();
    /// let mut on_id = None;
    ///
    /// AlphaNetworkBuilder::new(&mut network)
    ///     .create_entrypoint_node()
    ///     .create_objecttype_node("Object", &mut on_id)
    ///     .create_alphatermination_node()
    ///     .collect()
    /// ;
    /// ```
    pub fn create_alphatermination_node(mut self) -> Self {
        let parent_id = self
            .last_alphanode
            .expect("AlphaTerminationNode should come after a Node");

        let atn_id = self.network.alphanodes.new_node(
            PhreakAlphaNode::AlphaTerminalNode(Default::default())
        );
        parent_id.append(atn_id, &mut self.network.alphanodes);

        self.last_alphanode = Some(atn_id);
        self
    }

}
