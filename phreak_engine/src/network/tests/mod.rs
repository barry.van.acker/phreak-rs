use phreak_facts::{FactsBuilder, Value};

use crate::config::GeneralConfig;
use crate::evaluation::evaluate;
use crate::memory::KnowledgeSession;
use crate::network::builder::alpha_builder::AlphaNetworkBuilder;
use crate::network::builder::beta_builder::BetaNetworkBuilder;
use crate::network::conditions::{Condition, JoinConstraint, Operator};
use crate::network::Network;
use crate::network::nodes::PhreakBetaNode;
use crate::print::{print_alpha_network, print_beta_network};
use crate::store::{FactHandle, Store};

fn helper_create_fact_car(store: &mut Store) -> FactHandle {
    let fact_car = FactsBuilder::new("Car".to_owned())
        .add_attributes("color".to_string(), "red".to_string())
        .add_attributes("type".to_string(), "hatchback".to_string())
        .add_attributes("brand".to_string(), "Toyota".to_string())
        .build();
    store.add_fact(fact_car)
}

fn helper_create_fact_tree(store: &mut Store) -> FactHandle {
    let fact_tree = FactsBuilder::new("Tree".to_owned())
        .add_attributes("kind".to_string(), "Maple".to_string())
        .add_attributes("genus".to_string(), "Acer".to_string())
        .add_attributes("color".to_string(), "red".to_string())
        .build();

    store.add_fact(fact_tree)
}

fn helper_create_fact_driver(store: &mut Store, car: &FactHandle) -> FactHandle {
    let fact_car = FactsBuilder::new("Driver".to_owned())
        .add_attributes("name".to_string(), "Vettel".to_string())
        .add_values(From::from("drives"), From::from(car.get_index()))
        .build();
    store.add_fact(fact_car)
}

/// Create a network structure for testing if the given input is a certain object type.
///
#[test]
fn structuretest_object_filter() {
    let mut network = Network::new();
    let mut ks = KnowledgeSession::new();
    let config = GeneralConfig::default();

    let mut on_id = None;
    let (start_id, end_id) = AlphaNetworkBuilder::new(&mut network)
        .create_entrypoint_node()
        .get_or_create_objecttype_node("Car", &mut on_id)
        .create_alphatermination_node()
        .collect()
        ;

    let mut pm_id = None;

    let pid = BetaNetworkBuilder::new(&mut network, &mut ks, &config)
        .start_partition(&mut pm_id)
        .start_path(start_id)
        .start_segment(pm_id.unwrap())
        .collect(start_id, end_id);

    let h1 = helper_create_fact_car(&mut ks.fact_store);
    let h2 = helper_create_fact_tree(&mut ks.fact_store);

    let start = network.alphanodes[start_id].get();
    let pm = &mut ks.partition_memories[pm_id.unwrap()].lock().unwrap();

    network
        .partitions[pid].assert_object(h1, &network, &mut ks.fact_store, pm);
    network
        .partitions[pid].assert_object(h2, &network, &mut ks.fact_store, pm);

    let json = serde_json::to_string_pretty(&network).unwrap();
    //println!("{}", json);
}


/// Create a network structure for testing if the given input is a certain object type.
///
#[test]
fn structuretest_attribute_filter() {
    let mut network = Network::new();
    let mut ks = KnowledgeSession::new();
    let config = GeneralConfig::default();

    let condition = Condition {
        field: Value::from("color"),
        operator: Operator::Equal,
        expr: Value::from("red"),
    };

    let mut on_id = None;
    let mut an_id = None;

    let (start_id, end_id) = AlphaNetworkBuilder::new(&mut network)
        .create_entrypoint_node()
        .get_or_create_objecttype_node("Car", &mut on_id)
        .get_or_create_alpha_node(condition, &mut an_id)
        .create_alphatermination_node()
        .collect()
        ;

    let mut pm_id = None;

    let pid = BetaNetworkBuilder::new(&mut network, &mut ks, &config)
        .start_partition(&mut pm_id)
        .start_path(start_id)
        .start_segment(pm_id.unwrap())
        .collect(start_id, end_id);

    let h1 = helper_create_fact_car(&mut ks.fact_store);
    let h2 = helper_create_fact_tree(&mut ks.fact_store);

    let start = network.alphanodes[start_id].get();
    let pm = &mut ks.partition_memories[pm_id.unwrap()].lock().unwrap();

    network
        .partitions[pid]
        .assert_object(h1, &network, &mut ks.fact_store, pm);
    network
        .partitions[pid]
        .assert_object(h2, &network, &mut ks.fact_store, pm);

    let json = serde_json::to_string_pretty(&network).unwrap();
    //println!("{}", json);
}

// Test the correlation between the LIAN node and the first betanode afterwards. This correlation
// is based upon facthandle.
#[test]
fn betajoinnode_lian_test() {
    let mut network = Network::new();
    let mut ks = KnowledgeSession::new();
    let config = GeneralConfig::default();

    let condition = Condition {
        field: Value::from("color"),
        operator: Operator::Equal,
        expr: Value::from("red"),
    };

    let mut on_id = None;
    let mut an_id = None;

    let (start_id, end_id) = AlphaNetworkBuilder::new(&mut network)
        .create_entrypoint_node()
        .get_or_create_objecttype_node("Car", &mut on_id)
        .get_or_create_alpha_node(condition, &mut an_id)
        .collect()
        ;

    let mut lian_id = None;
    let mut jn_id = None;

    let mut pm_id = None;

    let pid = BetaNetworkBuilder::new(&mut network, &mut ks, &config)
        .start_partition(&mut pm_id)
        .start_path(start_id)
        .start_segment(pm_id.unwrap())
        .get_or_create_lia_node(on_id.unwrap(), &mut lian_id)
        .get_or_create_betajoin_node(an_id.unwrap(), &mut jn_id, pm_id.unwrap(), JoinConstraint::ByHandle)
        .collect(start_id, end_id);

    let h1 = helper_create_fact_car(&mut ks.fact_store);
    let h2 = helper_create_fact_tree(&mut ks.fact_store);

    let start = network.alphanodes[start_id].get();
    {
        let pm = &mut ks.partition_memories[pm_id.unwrap()].lock().unwrap();

        let partition = &network.partitions[pid];
        partition.assert_object(h1, &network, &mut ks.fact_store, pm);
        println!("+++");
        partition.assert_object(h2, &network, &mut ks.fact_store, pm);
    }
    let json = serde_json::to_string_pretty(&network).unwrap();
    //println!("{}", json);

    if let PhreakBetaNode::JoinNode(b) = network.betanodes[jn_id.unwrap()].get() {
        let pm_id = pm_id.unwrap();
        let partition_memory = &*ks
            .partition_memories[pm_id].lock().unwrap();
        let bm = &partition_memory
            .beta_memories[b.betamemory_id];

        println!("{:?}", bm.staged_right_tuples);
        assert_eq!(bm.staged_right_tuples.get_insert_size(), 1, "We inserted one red Car.");
        assert_eq!(bm.staged_right_tuples.get_update_size(), 0);
        assert_eq!(bm.staged_right_tuples.get_delete_size(), 0);

        let segment = &network
            .segments[b.segment_id];

        let sm = &partition_memory
            .segment_memories[segment.segmentmemory_id];

        assert_eq!(sm.staged_left_tuples.get_insert_size(), 1, "We inserted one red Car.");
        assert_eq!(sm.staged_left_tuples.get_update_size(), 0);
        assert_eq!(sm.staged_left_tuples.get_delete_size(), 0);
    }

    evaluate(&network, &ks);
}

// Test a join of two facts, based on the equality operation
#[test]
fn betajoinnode_equal_test() {
    let mut network = Network::new();
    let mut ks = KnowledgeSession::new();
    let config = GeneralConfig::default();

    let mut on_car_id = None;
    let mut on_driver_id = None;
    let mut an_red_car_id = None;

    let (start_alpha_id, end_red_car_id) = AlphaNetworkBuilder::new(&mut network)
        .create_entrypoint_node()
        .get_or_create_objecttype_node("Car", &mut on_car_id)
        .get_or_create_alpha_node(
            Condition {
                field: Value::from("color"),
                operator: Operator::Equal,
                expr: Value::from("red"),
            },
            &mut an_red_car_id,
        )
        .collect()
        ;

    let (_, end_red_car_id) = AlphaNetworkBuilder::new(&mut network)
        .set_entrypoint_node(start_alpha_id)
        .get_or_create_objecttype_node("Driver", &mut on_driver_id)
        .collect()
        ;

    let mut lian_id = None;
    let mut jn_driver_id = None;
    let mut jn_car_id = None;

    let mut pm_id = None;

    // Find any driver
    // that drives a red car
    let pid = BetaNetworkBuilder::new(&mut network, &mut ks, &config)
        .start_partition(&mut pm_id)
        .start_path(start_alpha_id)
        .start_segment(pm_id.unwrap())
        .get_or_create_lia_node(on_driver_id.unwrap(), &mut lian_id)
        .get_or_create_betajoin_node(on_driver_id.unwrap(), &mut jn_driver_id,
                                     pm_id.unwrap(), JoinConstraint::ByHandle)
        .get_or_create_betajoin_node(
            an_red_car_id.unwrap(), &mut jn_car_id, pm_id.unwrap(),
            JoinConstraint::LeftFieldReference(Operator::Equal, "drives".to_string()),
        )
        .collect(start_alpha_id, end_red_car_id);

    let h1 = helper_create_fact_car(&mut ks.fact_store);
    let h2 = helper_create_fact_tree(&mut ks.fact_store);
    let h3 = helper_create_fact_driver(&mut ks.fact_store, &h1);

    let start = network.alphanodes[start_alpha_id].get();
    {
        let pm = &mut ks.partition_memories[pm_id.unwrap()].lock().unwrap();

        let partition = &network.partitions[pid];
        partition.assert_object(h1, &network, &mut ks.fact_store, pm);
        println!("+++");
        partition.assert_object(h2, &network, &mut ks.fact_store, pm);
        println!("+++");
        partition.assert_object(h3, &network, &mut ks.fact_store, pm);
    }
    let json = serde_json::to_string_pretty(&network).unwrap();
    println!("{}", json);

    if let PhreakBetaNode::JoinNode(b) = network.betanodes[jn_car_id.unwrap()].get() {
        let pm_id = pm_id.unwrap();
        let partition_memory = &*ks
            .partition_memories[pm_id].lock().unwrap();
        let bm = &partition_memory
            .beta_memories[b.betamemory_id];

        println!("{:?}", bm.staged_right_tuples);
        assert_eq!(bm.staged_right_tuples.get_insert_size(), 1, "We inserted one red Car.");
        assert_eq!(bm.staged_right_tuples.get_update_size(), 0);
        assert_eq!(bm.staged_right_tuples.get_delete_size(), 0);

        let segment = &network
            .segments[b.segment_id];

        let sm = &partition_memory
            .segment_memories[segment.segmentmemory_id];

        println!("{:?}", sm.staged_left_tuples);
        assert_eq!(sm.staged_left_tuples.get_insert_size(), 1, "The Driver drives the red Car.");
        assert_eq!(sm.staged_left_tuples.get_update_size(), 0);
        assert_eq!(sm.staged_left_tuples.get_delete_size(), 0);
    }

    print_alpha_network(&network);
    print_beta_network(&network);
    evaluate(&network, &ks);

}
