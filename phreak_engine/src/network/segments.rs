use serde::{Deserialize, Serialize};

use crate::memory::SegmentMemoryId;
use crate::network::PhreakBetaNodeId;

#[derive(Deserialize, Serialize)]
pub struct Segment {
    pub(crate) segmentmemory_id: SegmentMemoryId,
    pub(crate) nodes: Vec<PhreakBetaNodeId>,
}

impl Segment {
    pub fn new(segmentmemory_id: SegmentMemoryId) -> Self {
        Segment { segmentmemory_id, nodes: Vec::with_capacity(10) }
    }
}