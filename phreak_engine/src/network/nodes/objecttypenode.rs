use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::PartitionMemory;
use crate::memory::tuples::RightTuple;
use crate::network::{Network, PhreakAlphaNodeId};
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};

#[derive(Debug, Deserialize, Serialize)]
pub struct ObjectTypeNode {
    object_name: String,
}

impl ObjectTypeNode {
    pub fn get_object_name(&self) -> &str {
        self.object_name.as_str()
    }

    pub fn retract_right_tuples(&self,
                                handle: FactHandle,
                                network: &Network,
                                store: &Store,
                                pm: &mut PartitionMemory,
    ) {
        let tmp: Vec<_>;

        tmp = handle
            .right_tuples(store)
            .collect();

        tmp.iter().for_each(|(pid, rt_id)|
            RightTuple::retract_tuple(*pid, *rt_id, &network, store, pm));
    }

    pub fn retract_left_tuples(&self,
                               handle: FactHandle,
                               network: &Network,
                               store: &Store,
                               pm: &mut PartitionMemory,
    ) {
        todo!()
    }
}

impl Default for ObjectTypeNode {
    fn default() -> Self {
        ObjectTypeNode { object_name: String::new() }
    }
}

impl From<String> for ObjectTypeNode {
    fn from(object_name: String) -> Self {
        ObjectTypeNode { object_name }
    }
}

impl From<&str> for ObjectTypeNode {
    fn from(object_name: &str) -> Self {
        ObjectTypeNode { object_name: String::from(object_name) }
    }
}

impl ObjectPropagator for ObjectTypeNode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("ObjectTypeNode assert_object {:?}", handle);
        if self.object_name == store[handle].get_name() {
            self_id
                .children(&network.alphanodes)
                .for_each(|child_id| network.alphanodes[child_id].get()
                    .assert_object(child_id, handle, network, store, pm)
                );
        }
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        if self.object_name == store[handle].get_name() {
            self_id
                .children(&network.alphanodes)
                .for_each(|child_id| network.alphanodes[child_id].get()
                    .modify_object(child_id, handle, network, store, pm)
                );
        }
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        self.retract_right_tuples(handle, network, store, pm);
        self.retract_left_tuples(handle, network, store, pm);
    }
}
