use serde::{Deserialize, Serialize};

use crate::memory::{BetaMemoryId, PartitionMemoryId, KnowledgeSession};
use crate::network::{PhreakAlphaNodeId, PhreakBetaNodeId, SegmentId, Network};
use crate::memory::tuples::RightTuple;
use crate::network::conditions::JoinConstraint;

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinNode {
    pub(crate) betamemory_id: BetaMemoryId,
    pub(crate) segment_id: SegmentId,
    pub(crate) partition_id: PartitionMemoryId,
    pub(crate) rightbridgenode_id: PhreakBetaNodeId,
    pub(crate) constraint: JoinConstraint,
}

impl JoinNode {
    pub fn new(
        betamemory_id: BetaMemoryId,
        rightbridgenode_id: PhreakBetaNodeId,
        segment_id: SegmentId,
        partition_id: PartitionMemoryId,
        constraint: JoinConstraint,
    ) -> Self {
        JoinNode { betamemory_id, segment_id, partition_id, rightbridgenode_id, constraint }
    }

    pub fn get_betamemory_id(&self) -> BetaMemoryId { self.betamemory_id }
    pub fn get_segment_id(&self) -> SegmentId { self.segment_id }
    pub fn get_partition_id(&self) -> PartitionMemoryId { self.partition_id }
    pub fn get_rightbridgenode_id(&self) -> PhreakAlphaNodeId { self.rightbridgenode_id }

    pub fn retract_right_tuple(
        &self, righttuple: &RightTuple, network: &Network, ks: &mut KnowledgeSession
    ) {
        todo!()
    }
}