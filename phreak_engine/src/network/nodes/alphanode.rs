use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::PartitionMemory;
use crate::network::{Network, PhreakAlphaNodeId};
use crate::network::conditions::Condition;
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};

#[derive(Debug, Serialize, Deserialize)]
pub struct AlphaNode {
    constraint: Condition,
}

impl AlphaNode {
    pub fn new(constraint: Condition) -> Self {
        AlphaNode { constraint }
    }

    fn is_allowed(&self, handle: FactHandle, store: &Store) -> bool {
        self.constraint.const_expression(&store[handle])
    }

    pub fn get_constraint(&self) -> &Condition { &self.constraint }
}

impl ObjectPropagator for AlphaNode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("AlphaNode assert_object {:?}", handle);
        if self.is_allowed(handle, store) {
            self_id
                .children(&network.alphanodes)
                .for_each(|child_id| {
                    network
                        .alphanodes[child_id].get()
                        .assert_object(child_id, handle, network, store, pm);
                });
        }
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        if self.is_allowed(handle, store) {
            self_id
                .children(&network.alphanodes)
                .for_each(|child_id| network.alphanodes[child_id].get()
                    .modify_object(child_id, handle, network, store, pm)
                );
        }
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        if self.is_allowed(handle, store) {
            self_id
                .children(&network.alphanodes)
                .for_each(|child_id| network.alphanodes[child_id].get()
                    .retract_object(child_id, handle, network, store, pm)
                );
        }
    }
}