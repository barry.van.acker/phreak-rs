use serde::{Deserialize, Serialize};
use crate::network::SegmentId;
use crate::memory::PartitionMemoryId;

#[derive(Debug, Deserialize, Serialize)]
pub struct BetaTerminalNode {
    pub(crate) segment_id: SegmentId,
    pub(crate) partition_id: PartitionMemoryId,
}