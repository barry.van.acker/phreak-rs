use serde::{Deserialize, Serialize};

use crate::network::{Network, PhreakAlphaNodeId};
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};
use crate::memory::partitionmemory::PartitionMemory;

#[derive(Debug, Deserialize, Serialize)]
pub struct EntryNode {}

impl Default for EntryNode {
    fn default() -> Self {
        EntryNode {}
    }
}

impl ObjectPropagator for EntryNode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("EntryNode assert_object {:?}", handle);
        self_id
            .children(&network.alphanodes)
            .for_each(|child_id| network.alphanodes[child_id].get()
                .assert_object(child_id, handle, network, store, pm)
            );
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        self_id
            .children(&network.alphanodes)
            .for_each(|child_id| network.alphanodes[child_id].get()
                .modify_object(child_id, handle, network, store, pm)
            );
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        self_id
            .children(&network.alphanodes)
            .for_each(|child_id| network.alphanodes[child_id].get()
                .retract_object(child_id, handle, network, store, pm)
            );
    }
}