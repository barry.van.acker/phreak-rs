use serde::{Deserialize, Serialize};
use smallvec::SmallVec;

use crate::memory::partitionmemory::PartitionMemory;
use crate::network::{Network, PhreakAlphaNodeId, PhreakBetaNodeId};
use crate::network::nodes::PhreakBetaNode;
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};

/// RightBridgeNode translates an ObjectPropagation into a RightTuple and stages
/// it in a betanetwork node.
#[derive(Debug, Deserialize, Serialize)]
pub struct RightBridgeNode {
    betanodes: SmallVec<[PhreakBetaNodeId; 3]>,
}

impl RightBridgeNode {
    pub fn add_betanode(&mut self, id: PhreakBetaNodeId) {
        self.betanodes.push(id);
    }
}

impl Default for RightBridgeNode {
    fn default() -> Self {
        RightBridgeNode { betanodes: SmallVec::new() }
    }
}

impl ObjectPropagator for RightBridgeNode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("RightBridgeNode assert_object {:?}", handle);
        self.betanodes.iter()
            .for_each(|&betanode_id| match network.betanodes[betanode_id].get() {
                PhreakBetaNode::JoinNode(n) => {
                    let (right_tuple_id, right_tuple) =
                        PartitionMemory::create_right_tuple(
                            &mut pm.right_tuples, handle, betanode_id,
                        );

                    let _was_empty = pm
                        .beta_memories[n.get_betamemory_id()]
                        .get_staged_right_tuples_mut()
                        .add_insert(right_tuple_id, right_tuple);
                }
                PhreakBetaNode::BetaTerminalNode(n) => { unreachable!() }
            });
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }
}