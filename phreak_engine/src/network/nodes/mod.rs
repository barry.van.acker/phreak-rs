use serde::{Deserialize, Serialize};

use crate::memory::{BetaMemoryId, KnowledgeSession};
use crate::memory::partitionmemory::{PartitionMemory, RightTupleId};
use crate::network::{Network, PartitionId, PhreakAlphaNodeId, PhreakBetaNodeId, SegmentId};
use crate::network::nodes::alphanode::AlphaNode;
use crate::network::nodes::alphaterminalnode::AlphaTerminalNode;
use crate::network::nodes::betaterminalnode::BetaTerminalNode;
use crate::network::nodes::entrynode::EntryNode;
use crate::network::nodes::joinnode::JoinNode;
use crate::network::nodes::lianode::LIANode;
use crate::network::nodes::objecttypenode::ObjectTypeNode;
use crate::network::nodes::rightbridgenode::RightBridgeNode;
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};

pub mod alphanode;
pub mod alphaterminalnode;
pub mod lianode;
pub mod entrynode;
pub mod objecttypenode;
pub mod rightbridgenode;

pub mod joinnode;
pub mod betaterminalnode;

/// A node in the Phreak alpha network.
///
/// The Alpha network differs from the Beta network in that it performs
/// eager processing, filtering each object completely until either it has
/// reached the leaf nodes, or it was filtered out, before taking on the
/// next object.
#[derive(Debug, Deserialize, Serialize)]
pub enum PhreakAlphaNode {
    /// An entrypoint node is the startingpoint for the alpha network
    EntryNode(EntryNode),

    /// An ObjectTypeNode provides filtering on Object in the alpha network
    ObjectTypeNode(ObjectTypeNode),

    /// An AlphaNode provides filtering on attributes of the object
    AlphaNode(AlphaNode),

    /// An AlphaTerminalNode is used when a rule doesn't require multi-object matching
    AlphaTerminalNode(AlphaTerminalNode),

    /// Bridge from the alpha network to the betanetwork for left tuples
    LIANode(LIANode),

    /// Bridge from the alpha to betanetwork for right tuples
    RightBridgeNode(RightBridgeNode),
}

impl PhreakAlphaNode {
    pub fn is_entrynode(&self) -> bool {
        match self {
            PhreakAlphaNode::EntryNode(_) => true,
            _ => false,
        }
    }
    pub fn is_objecttypenode(&self) -> bool {
        match self {
            PhreakAlphaNode::ObjectTypeNode(_) => true,
            _ => false,
        }
    }
    pub fn is_alphanode(&self) -> bool {
        match self {
            PhreakAlphaNode::AlphaNode(_) => true,
            _ => false,
        }
    }
    pub fn is_alphaterminalnode(&self) -> bool {
        match self {
            PhreakAlphaNode::AlphaTerminalNode(_) => true,
            _ => false,
        }
    }
    pub fn is_lianode(&self) -> bool {
        match self {
            PhreakAlphaNode::LIANode(_) => true,
            _ => false,
        }
    }

    pub fn is_rbnode(&self) -> bool {
        match self {
            PhreakAlphaNode::RightBridgeNode(_) => true,
            _ => false,
        }
    }

    pub fn type_name(&self) -> &str {
        match self {
            PhreakAlphaNode::EntryNode(_) => { "entrynode" }
            PhreakAlphaNode::ObjectTypeNode(_) => { "objecttypenode" }
            PhreakAlphaNode::AlphaNode(_) => { "alphanode" }
            PhreakAlphaNode::AlphaTerminalNode(_) => { "alphaterminalnode" }
            PhreakAlphaNode::LIANode(_) => { "lianode" }
            PhreakAlphaNode::RightBridgeNode(_) => { "rightbridgenode" }
        }
    }
}

impl ObjectPropagator for PhreakAlphaNode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        match self {
            PhreakAlphaNode::EntryNode(n) => { n.assert_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::ObjectTypeNode(n) => { n.assert_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaNode(n) => { n.assert_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaTerminalNode(n) => { n.assert_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::LIANode(n) => { n.assert_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::RightBridgeNode(n) => { n.assert_object(self_id, handle, network, store, pm) }
        }
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        match self {
            PhreakAlphaNode::EntryNode(n) => { n.modify_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::ObjectTypeNode(n) => { n.modify_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaNode(n) => { n.modify_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaTerminalNode(n) => { n.modify_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::LIANode(n) => { n.modify_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::RightBridgeNode(n) => { n.modify_object(self_id, handle, network, store, pm) }
        }
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        match self {
            PhreakAlphaNode::EntryNode(n) => { n.retract_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::ObjectTypeNode(n) => { n.retract_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaNode(n) => { n.retract_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::AlphaTerminalNode(n) => { n.retract_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::LIANode(n) => { n.retract_object(self_id, handle, network, store, pm) }
            PhreakAlphaNode::RightBridgeNode(n) => { n.retract_object(self_id, handle, network, store, pm) }
        }
    }
}

/// Just the type, without the content
#[derive(Debug, Deserialize, Serialize, Clone, Copy)]
pub enum PhreakBetaNodeType {
    JoinNode,
    BetaTerminalNode,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum PhreakBetaNode {
    JoinNode(JoinNode),
    BetaTerminalNode(BetaTerminalNode),
}

impl PhreakBetaNode {
    pub fn get_type(&self) -> PhreakBetaNodeType {
        match self {
            PhreakBetaNode::JoinNode(_) => PhreakBetaNodeType::JoinNode,
            PhreakBetaNode::BetaTerminalNode(_) => PhreakBetaNodeType::BetaTerminalNode,
        }
    }

    pub fn retract_right_tuple(pid: PartitionId, nid: PhreakBetaNodeId, rid: RightTupleId, network: &Network, ks: &mut KnowledgeSession) {
        // match self {
        //     PhreakBetaNode::JoinNode(n) => {
        //         n.retract_right_tuple(righttuple, network, ks);
        //     }
        //     PhreakBetaNode::BetaTerminalNode(_) => { unreachable!() }
        // }
        todo!()
    }

    pub fn get_partition_id(&self) -> PartitionId {
        match self {
            PhreakBetaNode::JoinNode(n) => { n.partition_id }
            PhreakBetaNode::BetaTerminalNode(n) => { n.partition_id }
        }
    }

    pub fn get_segment_id(&self) -> SegmentId {
        match self {
            PhreakBetaNode::JoinNode(n) => { n.segment_id }
            PhreakBetaNode::BetaTerminalNode(n) => { n.segment_id }
        }
    }

    pub fn set_segment_id(&mut self, id: SegmentId) {
        match self {
            PhreakBetaNode::JoinNode(n) => { n.segment_id = id; }
            PhreakBetaNode::BetaTerminalNode(n) => { n.segment_id = id; }
        }
    }

    pub fn get_betamemory_id(&self) -> Option<BetaMemoryId> {
        match self {
            PhreakBetaNode::JoinNode(n) => { Some(n.betamemory_id) }
            PhreakBetaNode::BetaTerminalNode(_) => { None }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::mem::size_of;

    use super::*;

    #[test]
    fn alphanode_sizes() {
        // println!("EntryNode {}", size_of::<EntryNode>());
        // println!("AlphaNode {}", size_of::<AlphaNode>());
        // println!("AlphaTerminalNode {}", size_of::<AlphaTerminalNode>());
        // println!("ObjectTypeNode {}", size_of::<ObjectTypeNode>());
        // println!("LIANode {}", size_of::<LIANode>());
        // println!("RightBridgeNode {}", size_of::<RightBridgeNode>());

        // AlphaNode is the larges structure when we consider only required data.
        // AlphaNode is our upper bound on the size, meaning that other nodes are free
        // to optimize for speed over size, as long as they stay below AlphaNode, and it will
        // have no impact on the size of the memory needed for the treenodes.
        //
        // These hold for my develop environment, might need tweaking on other architectures
        assert!(size_of::<EntryNode>() <= size_of::<AlphaNode>());
        assert!(size_of::<AlphaTerminalNode>() <= size_of::<AlphaNode>());
        assert!(size_of::<ObjectTypeNode>() <= size_of::<AlphaNode>());
        assert!(size_of::<LIANode>() <= size_of::<AlphaNode>());
        assert!(size_of::<RightBridgeNode>() <= size_of::<AlphaNode>());
    }
}