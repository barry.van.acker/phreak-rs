use serde::{Deserialize, Serialize};

use crate::network::{Network, PhreakAlphaNodeId};
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};
use crate::memory::partitionmemory::PartitionMemory;

/// This needs to be provided by the instantiator
#[derive(Debug, Serialize, Deserialize)]
pub struct Callback {}

impl Callback {
    fn f(&self, handle: FactHandle) {
        println!("FIRE {:?}", handle);
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct AlphaTerminalNode {
    insert_callback: Option<Callback>,
}

impl Default for AlphaTerminalNode {
    fn default() -> Self {
        AlphaTerminalNode { insert_callback: None }
    }
}

impl ObjectPropagator for AlphaTerminalNode
{
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("AlphaTerminalNode assert_object {:?}", handle);
        match &self.insert_callback {
            None => {}
            Some(callback) => { callback.f(handle) }
        }
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }
}