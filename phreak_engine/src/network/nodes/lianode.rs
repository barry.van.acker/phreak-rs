use serde::{Deserialize, Serialize};
use smallvec::SmallVec;

use crate::memory::partitionmemory::PartitionMemory;
use crate::network::{Network, PhreakAlphaNodeId, PhreakBetaNodeId};
use crate::network::nodes::PhreakBetaNode;
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};

/// LeftInput Adapter Node translates an ObjectPropagation into a LeftTuple and stages
/// it in a betanetwork node.
#[derive(Debug, Deserialize, Serialize)]
pub struct LIANode {
    betanodes: SmallVec<[PhreakBetaNodeId; 3]>,
}

impl LIANode {
    pub fn add_betanode(&mut self, id: PhreakBetaNodeId) {
        self.betanodes.push(id);
    }
}

impl Default for LIANode {
    fn default() -> Self {
        LIANode { betanodes: SmallVec::new() }
    }
}

impl ObjectPropagator for LIANode {
    fn assert_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        // println!("LiaNode assert_object {:?}", handle);
        self.betanodes.iter()
            .for_each(|&betanode_id| match network.betanodes[betanode_id].get() {
                PhreakBetaNode::JoinNode(n) => {
                    let (left_tuple_id, left_tuple) =
                        PartitionMemory::create_left_tuple(handle, &mut pm.left_tuples);

                    let was_empty = pm
                        .segment_memories[n.get_segment_id()]
                        .staged_left_tuples
                        .add_insert(left_tuple_id, left_tuple);
                }
                PhreakBetaNode::BetaTerminalNode(n) => { unreachable!() }
            });
    }

    fn modify_object(&self, self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }

    fn retract_object(&self, self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    ) {
        unimplemented!()
    }
}