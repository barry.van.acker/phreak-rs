use crate::network::{Network, PhreakAlphaNodeId};
use crate::store::{FactHandle, Store};
use crate::memory::partitionmemory::PartitionMemory;

/// Nodes that implement this trait are capable of handling
/// facts based upon conditions that do not relay on outside
/// dependencies. A fact object can be matched on it's class
/// and it's attributes, but it can't be compared to other
/// fact objects.
pub trait ObjectPropagator {
    /// A new fact object is asserted
    fn assert_object(&self,
                     self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    );
    /// The given fact object has been changed
    fn modify_object(&self,
                     self_id: PhreakAlphaNodeId,
                     handle: FactHandle,
                     network: &Network,
                     store: &Store,
                     pm: &mut PartitionMemory,
    );
    /// A fact object has been revoked
    fn retract_object(&self,
                      self_id: PhreakAlphaNodeId,
                      handle: FactHandle,
                      network: &Network,
                      store: &Store,
                      pm: &mut PartitionMemory,
    );
}