use std::collections::HashSet;

use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::PartitionMemory;
use crate::memory::PartitionMemoryId;
use crate::network::{Network, PathId, PhreakAlphaNodeId};
use crate::network::object_propagation::ObjectPropagator;
use crate::store::{FactHandle, Store};
use generational_arena::Arena;
use crate::network::paths::Path;

#[derive(Deserialize, Serialize)]
pub struct Partition {
    pub(crate) partitionmemory_id: PartitionMemoryId,
    pub(crate) paths: Vec<PathId>,

    // Cache of unique entrypoints of all paths in the partition.
    pub(crate) entrynodes: HashSet<PhreakAlphaNodeId>,
}

impl Partition {
    pub fn new(partitionmemory_id: PartitionMemoryId) -> Self {
        Partition {
            partitionmemory_id,
            paths: Vec::with_capacity(10),
            entrynodes: HashSet::with_capacity(10),
        }
    }

    /// Call after each change, such as adding or removing a rule
    pub fn recalculate_entrynodes(&mut self, paths: &Arena<Path>) {
        let entrynodes = &mut self.entrynodes;
        entrynodes.clear();
        self.paths
            .iter()
            .for_each(|&path_id| {
                entrynodes.insert(paths[path_id].entrypoint_node);
            });
    }

    pub fn assert_object(&self,
                         handle: FactHandle,
                         network: &Network,
                         store: &Store,
                         pmem: &mut PartitionMemory,
    ) {
        self.entrynodes
            .iter()
            .for_each(|&entrynode_id|
                network
                    .alphanodes[entrynode_id]
                    .get()
                    .assert_object(entrynode_id, handle, network, store, pmem)
            );
    }
}