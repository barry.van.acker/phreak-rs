use std::cmp::Ordering;

use generational_arena::{Arena, Index};
use generational_indextree::Arena as TreeArena;
use serde::{Deserialize, Serialize};

use phreak_facts::{FactObject, Value};

use crate::config::*;
use crate::memory::tuples::{LeftTuple, RightTuple};
use crate::network::builder::memory_builder::FieldIndex;
use crate::network::nodes::PhreakBetaNodeType;
use crate::store::{FactHandle, Store};

/// ConstraintType determines what is the most optimal type of storage for the tuples in
/// this relation.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum ConstraintType {
    Equal,
    NotEqual,
    GreaterThan,
    GreaterEqual,
    LessThan,
    LessEqual,
    Range,
    Unknown,
}

impl ConstraintType {
    pub fn new_from_string(op: &str) -> ConstraintType {
        match op {
            "==" => ConstraintType::Equal,
            "!=" => ConstraintType::NotEqual,
            ">" => ConstraintType::GreaterThan,
            ">=" => ConstraintType::GreaterEqual,
            "<" => ConstraintType::LessThan,
            "<=" => ConstraintType::LessEqual,
            _ => ConstraintType::Unknown,
        }
    }

    pub fn new_from_operator(op: &Operator) -> ConstraintType {
        match op {
            Operator::Equal => ConstraintType::Equal,
            Operator::NotEqual => ConstraintType::NotEqual,
            Operator::LessThan => ConstraintType::LessThan,
            Operator::LessEqual => ConstraintType::LessEqual,
            Operator::MoreThan => ConstraintType::GreaterThan,
            Operator::MoreEqual => ConstraintType::GreaterEqual,
        }
    }

    pub fn is_comparison(&self) -> bool {
        match self {
            ConstraintType::LessThan | ConstraintType::LessEqual |
            ConstraintType::GreaterThan | ConstraintType::GreaterEqual => true,
            _ => false
        }
    }

    pub fn is_equality(&self) -> bool {
        match self {
            ConstraintType::Equal | ConstraintType::NotEqual => true,
            _ => false
        }
    }

    pub fn is_ascending(&self) -> bool {
        match self {
            ConstraintType::GreaterThan | ConstraintType::GreaterEqual => true,
            _ => false
        }
    }

    pub fn is_descending(&self) -> bool {
        match self {
            ConstraintType::LessThan | ConstraintType::LessEqual => true,
            _ => false
        }
    }

    pub fn is_indexable(&self) -> bool {
        match self {
            ConstraintType::Equal |
            ConstraintType::GreaterThan | ConstraintType::GreaterEqual |
            ConstraintType::LessThan | ConstraintType::LessEqual |
            ConstraintType::Range => true,
            _ => false,
        }
    }

    pub fn is_indexable_for_node(&self, node_type: PhreakBetaNodeType) -> bool {
        match self {
            ConstraintType::Equal => true,
            ConstraintType::NotEqual | ConstraintType::Unknown => false,
            _ => USE_COMPARISON_INDEX && false // todo: (nodeType == NodeTypeEnums.NotNode || nodeType == NodeTypeEnums.ExistsNode);
        }
    }
}


#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum JoinConstraint {
    /// Used by first join after LIAnode, to efficiently match the RightTuple to
    /// the LeftTuple of the same object.
    ByHandle,
    /// Apply the constraint on the given attribute of the left object and use the identity of the right object
    LeftFieldReference(Operator, String),
}

impl JoinConstraint {
    pub fn evaluate(&self, left_tuple: &LeftTuple, handle: FactHandle,
                    left_arena: &TreeArena<LeftTuple>, right_arena: &Arena<RightTuple>,
                    store: &Store,
    ) -> bool {
        let b =
            match self {
                JoinConstraint::ByHandle => left_tuple.fact_handle == handle,
                JoinConstraint::LeftFieldReference(operator, attribute_name) => {
                    let left = &store[left_tuple.fact_handle];

                    left.get_attribute(attribute_name.as_str())
                        .map_or(
                            false,
                            |attr| operator.execute(
                                &attr.1, &Value::from(Index::from(&handle)),
                            ),
                        )
                }
            };
        //println!("evaluate {:?} --> {}", self, b);
        b
    }

    pub fn get_constraint_type(&self) -> ConstraintType {
        match self {
            JoinConstraint::ByHandle => ConstraintType::Equal,
            JoinConstraint::LeftFieldReference(op, _) => {
                match op {
                    Operator::Equal => ConstraintType::Equal,
                    Operator::NotEqual => ConstraintType::NotEqual,
                    Operator::LessThan => ConstraintType::LessThan,
                    Operator::LessEqual => ConstraintType::LessEqual,
                    Operator::MoreThan => ConstraintType::GreaterThan,
                    Operator::MoreEqual => ConstraintType::GreaterEqual,
                }
            }
        }
    }

    /// true if we can use an index to speed up the evaluation
    pub fn is_indexable(&self) -> bool {
        match self {
            JoinConstraint::ByHandle => true,
            JoinConstraint::LeftFieldReference(op, _) => {
                match op {
                    Operator::Equal | Operator::NotEqual => true,
                    _ => false
                }
            }
        }
    }

    pub fn is_unification(&self) -> bool {
        false
    }

    pub fn get_field_index(&self) -> FieldIndex {
        match self {
            JoinConstraint::ByHandle =>
                FieldIndex::Identity,
            JoinConstraint::LeftFieldReference(op, attr_name) =>
                FieldIndex::Field(From::from(attr_name.as_str())),
        }
    }
}

/// A condition is something to test for
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Condition {
    pub field: Value,
    pub operator: Operator,
    pub expr: Value,
}

/// An operator defines a testing directive
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum Operator {
    Equal,
    NotEqual,
    LessThan,
    LessEqual,
    MoreThan,
    MoreEqual,
}

impl Condition {
    /// Treats conditions second field "expr" as a const expression
    pub fn const_expression(&self, obj: &FactObject) -> bool {
        match obj
            .iter_attrs()
            .find(|&attr| attr.0 == self.field)
        {
            Some(attr) => self.operator.execute(&attr.1, &self.expr),
            None => false
        }
    }
}

impl Operator {
    /// Execute this operator against the left and right value
    ///
    /// ```
    /// # use phreak_facts::Value;
    /// use phreak_engine::network::conditions::Operator;
    ///
    /// let oper = Operator::Equal;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(22)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), false);
    ///
    /// let oper = Operator::LessEqual;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), true);
    /// assert_eq!(oper.execute(&Value::from(5),&Value::from(10)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(5)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), false);
    ///
    /// let oper = Operator::LessThan;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), false);
    /// assert_eq!(oper.execute(&Value::from(5),&Value::from(10)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(5)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), false);
    ///
    /// let oper = Operator::MoreEqual;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), true);
    /// assert_eq!(oper.execute(&Value::from(5),&Value::from(10)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(5)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), false);
    ///
    /// let oper = Operator::MoreThan;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), false);
    /// assert_eq!(oper.execute(&Value::from(5),&Value::from(10)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(5)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), false);
    ///
    /// let oper = Operator::NotEqual;
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(10)), false);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from(22)), true);
    /// assert_eq!(oper.execute(&Value::from(10),&Value::from("10")), true);
    /// ```
    pub fn execute(&self, left: &Value, right: &Value) -> bool {
        match self {
            Operator::Equal => {
                left.typed_eq(right)
            }
            Operator::NotEqual => {
                !left.typed_eq(right)
            }
            Operator::LessThan => {
                left.typed_cmp(right) == Some(Ordering::Less)
            }
            Operator::LessEqual => {
                let c = left.typed_cmp(right);
                c == Some(Ordering::Less) || c == Some(Ordering::Equal)
            }
            Operator::MoreThan => {
                left.typed_cmp(right) == Some(Ordering::Greater)
            }
            Operator::MoreEqual => {
                let c = left.typed_cmp(right);
                c == Some(Ordering::Greater) || c == Some(Ordering::Equal)
            }
        }
    }
}