use serde::{Deserialize, Serialize};

use crate::network::{PhreakAlphaNodeId, SegmentId, PhreakBetaNodeId};

/// Id of the last node in the Path
#[derive(Deserialize, Serialize)]
pub enum TipNode {
    /// Needed for initial value
    None,

    /// Id for AlphaTerminalNode
    Alpha(PhreakAlphaNodeId),

    /// Id for BetaTerminalNode
    Beta(PhreakBetaNodeId),
}

#[derive(Deserialize, Serialize)]
pub struct Path {
    pub(crate) entrypoint_node: PhreakAlphaNodeId,
    pub(crate) tip_node: TipNode,

    pub(crate) segments: Vec<SegmentId>,
}

impl Path {
    pub fn new(entrypoint_node: PhreakAlphaNodeId) -> Self {
        Path { entrypoint_node, tip_node: TipNode::None, segments: Vec::with_capacity(10) }
    }
}