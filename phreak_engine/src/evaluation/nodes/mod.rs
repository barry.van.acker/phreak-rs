use generational_arena::Arena;
use generational_indextree::Arena as TreeArena;

use crate::memory::betamemory::BetaMemory;
use crate::memory::collections::tuplesets::TupleSets;
use crate::memory::partitionmemory::LeftTupleId;
use crate::memory::tuples::{LeftTuple, RightTuple};
use crate::network::Network;
use crate::network::nodes::PhreakBetaNode;
use crate::network::partitions::Partition;
use crate::store::Store;

pub mod joinnode;

pub fn switch_on_betanode(betanode: &PhreakBetaNode,
                          bm: &mut BetaMemory,
                          src_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                          trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                          network: &Network,
                          part: &Partition,
                          left_tuple_arena: &mut TreeArena<LeftTuple>,
                          right_tuple_arena: &mut Arena<RightTuple>,
                          store: &Store,
) {
    match betanode {
        PhreakBetaNode::JoinNode(node) =>
            joinnode::do_node(node, bm,
                              src_left_tuples, trg_left_tuples,
                              network, part,
                              left_tuple_arena, right_tuple_arena,
                              store),
        PhreakBetaNode::BetaTerminalNode(_) => {}
    }
}