use generational_arena::Arena;
use generational_indextree::Arena as TreeArena;

use crate::memory::betamemory::BetaMemory;
use crate::memory::collections::tuplesets::TupleSets;
use crate::memory::partitionmemory::{LeftTupleId, RightTupleId};
use crate::memory::tuplememory::TupleMemory;
use crate::memory::tuples::{LeftTuple, RightTuple, StagedType, Tuple};
use crate::network::Network;
use crate::network::nodes::joinnode::JoinNode;
use crate::network::partitions::Partition;
use crate::store::Store;

pub fn do_node(join_node: &JoinNode,
               bm: &mut BetaMemory,
               src_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
               trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
               // staged_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>, needed later??
               network: &Network,
               part: &Partition,
               left_tuple_arena: &mut TreeArena<LeftTuple>,
               right_tuple_arena: &mut Arena<RightTuple>,
               store: &Store,
) {
    // takeAll
    let mut src_right_tuples = TupleSets::<RightTuple, RightTupleId>::new();
    std::mem::swap(&mut src_right_tuples, bm.get_staged_right_tuples_mut());


    if !src_right_tuples.delete_is_empty() {
        do_right_deletes(bm, &src_right_tuples, trg_left_tuples /*stagedLeftTuples*/);
    }

    if !src_left_tuples.delete_is_empty() {
        do_left_deletes(bm, src_left_tuples, trg_left_tuples /*stagedLeftTuples*/);
    }

    // if !src_right_tuples.update_is_empty() {
    //     RuleNetworkEvaluator.doUpdatesReorderRightMemory(bm, &src_right_tuples);
    // }

    // if !src_left_tuples.update_is_empty() {
    //     RuleNetworkEvaluator.doUpdatesReorderLeftMemory(bm, src_left_tuples);
    // }

    // if !src_right_tuples.update_is_empty() {
    //     do_right_updates(join_node, bm, /*wm,*/ &src_right_tuples, trg_left_tuples /*stagedLeftTuples*/);
    // }

    // if !src_left_tuples.update_is_empty() {
    //     do_left_updates(join_node, bm, /*wm,*/ src_left_tuples, trg_left_tuples /*stagedLeftTuples*/);
    // }

    if !src_right_tuples.insert_is_empty() {
        do_right_inserts(join_node, bm, /*wm,*/
                         &src_right_tuples, trg_left_tuples,
                         left_tuple_arena, right_tuple_arena,
                         store,
        );
    }

    if !src_left_tuples.insert_is_empty() {
        do_left_inserts(join_node, bm, /*wm,*/
                        src_left_tuples, trg_left_tuples,
                        left_tuple_arena, right_tuple_arena,
                        store,
        );
    }

    src_right_tuples.reset_all();
    src_left_tuples.reset_all();
}

fn do_right_deletes(bm: &mut BetaMemory,
                    src_right_tuples: &TupleSets::<RightTuple, RightTupleId>,
                    trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                    /*stagedLeftTuples*/
) {}

fn do_left_deletes(bm: &mut BetaMemory,
                   src_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                   trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                   /*stagedLeftTuples*/
) {}

fn do_right_inserts(join_node: &JoinNode,
                    bm: &mut BetaMemory,
                    /*wm,*/
                    src_right_tuples: &TupleSets::<RightTuple, RightTupleId>,
                    trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                    left_tuple_arena: &mut TreeArena<LeftTuple>,
                    right_tuple_arena: &mut Arena<RightTuple>,
                    store: &Store,
) {
    let ltm = &mut bm.left_tuple_memory;
    let rtm = &mut bm.right_tuple_memory;

    // pre-allocate to reduce the number of alloc calls
    rtm.reserve(src_right_tuples.get_insert_size());

    let constraints = &join_node.constraint;

    for &right_tuple_id in src_right_tuples.iter_inserts() {
        rtm.add(right_tuple_id, right_tuple_arena, store);

        if !ltm.is_empty() {
            // try match
            let handle = right_tuple_arena[right_tuple_id].fact_handle;

            for left_tuple_id in ltm.iter()
            {
                let left_tuple = left_tuple_arena[left_tuple_id].get();
                if left_tuple.get_staged_type() == StagedType::Update {
                    // ignore, as it will get processed via left iteration. Children cannot be processed twice
                    continue;
                }

                if constraints.evaluate(left_tuple_arena[left_tuple_id].get(), handle,
                                        left_tuple_arena, right_tuple_arena,
                                        store,
                ) {
                    // see IndexEvaluator etc.
                    insert_child_left_tuple(left_tuple_id, right_tuple_id,
                                            trg_left_tuples,
                                            left_tuple_arena, right_tuple_arena,
                    );
                }
            }
        }

        right_tuple_arena[right_tuple_id].clear_staged();
    }
}

fn do_left_inserts(join_node: &JoinNode,
                   bm: &mut BetaMemory,
                   /*wm,*/
                   src_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                   trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                   left_tuple_arena: &mut TreeArena<LeftTuple>,
                   right_tuple_arena: &mut Arena<RightTuple>,
                   store: &Store,
) {
    let ltm = &mut bm.left_tuple_memory;
    let rtm = &mut bm.right_tuple_memory;
    let constraints = &join_node.constraint;

    for &left_tuple_id in src_left_tuples.iter_inserts() {
        ltm.add(left_tuple_id, left_tuple_arena, store);

        if !rtm.is_empty() {
            for right_tuple_id in rtm.iter() {
                let handle = right_tuple_arena[right_tuple_id].fact_handle;

                if constraints.evaluate(left_tuple_arena[left_tuple_id].get(), handle,
                                        left_tuple_arena, right_tuple_arena,
                                        store,
                ) {
                    insert_child_left_tuple(left_tuple_id, right_tuple_id,
                                            trg_left_tuples,
                                            left_tuple_arena, right_tuple_arena,
                    );
                }
            }
        }
        left_tuple_arena[left_tuple_id].get_mut().clear_staged();
    }
}

#[inline]
fn insert_child_left_tuple(left_tuple_id: LeftTupleId,
                           right_tuple_id: RightTupleId,
                           trg_left_tuples: &mut TupleSets<LeftTuple, LeftTupleId>,
                           left_tuple_arena: &mut TreeArena<LeftTuple>,
                           right_tuple_arena: &mut Arena<RightTuple>,
) {
    let expired =
        left_tuple_arena[left_tuple_id].get().expired ||
            right_tuple_arena[right_tuple_id].expired;

    if !expired {
        let new_id = left_tuple_arena
            .new_node(
                LeftTuple::new(right_tuple_arena[right_tuple_id].fact_handle, Some(right_tuple_id))
            );
        left_tuple_id.append(new_id, left_tuple_arena);

        trg_left_tuples.add_insert(new_id, left_tuple_arena[new_id].get_mut());
    }
}