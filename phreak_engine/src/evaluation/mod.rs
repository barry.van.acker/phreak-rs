//! This module holds the stack based evaluation code
use serde::{Deserialize, Serialize};

use crate::config::*;
use crate::evaluation::nodes::switch_on_betanode;
use crate::memory::collections::tuplesets::TupleSets;
use crate::memory::KnowledgeSession;
use crate::memory::partitionmemory::{LeftTupleId, PartitionMemory};
use crate::memory::pathmemory::PathMemory;
use crate::memory::tuples::LeftTuple;
use crate::network::{Network, SegmentId};
use crate::network::partitions::Partition;
use crate::network::paths::Path;

pub mod node_eval;
pub mod nodes;

/// Stack based evaluation
///
/// The smallest step in the evaluation consists of a single network segment.
#[derive(Debug, Deserialize, Serialize)]
pub struct StackEntry {
    pub(crate) segment_id: SegmentId,
    pub(crate) trg_tuples: TupleSets<LeftTuple, LeftTupleId>,
}


pub fn evaluate(network: &Network, ks: &KnowledgeSession) {
    for (pid, partition) in network.partitions.iter() {
        // lock the partition for exclusive access. Multiple partitions may be evaluated in parallel
        let mut guard = ks.partition_memories[partition.partitionmemory_id].lock().unwrap();
        evaluate_partition(
            partition, &mut *guard,
            network, ks,
        )
    }
}

/// Evaluate a partition of the network
///
/// Evaluate all paths in this partition of the network.
pub fn evaluate_partition(partition: &Partition, pmem: &mut PartitionMemory,
                          network: &Network, ks: &KnowledgeSession) {
    // for each path
    partition.paths
        .iter()
        .for_each(|&path_id| {
            // push all segments on the stack, back to front
            network.paths[path_id].segments
                .iter()
                .rev()
                .for_each(|&segment_id|
                    pmem.evaluation_stack.push(StackEntry { segment_id, trg_tuples: TupleSets::new() })
                );
        });

    // println!("stack: {:?}", pmem.evaluation_stack);

    evaluate_stack(ITERATION_COUNT, partition, pmem, network, ks);
}

/// Evaluate a single path in the network
pub fn evaluate_path(path: &Path, path_mem: &mut PathMemory,
                     partition: &Partition, pmem: &mut PartitionMemory,
                     network: &Network, ks: &KnowledgeSession)
{
    path.segments
        .iter()
        .rev()
        .for_each(|&segment_id|
            pmem.evaluation_stack.push(StackEntry { segment_id, trg_tuples: TupleSets::new() })
        );

    evaluate_stack(ITERATION_COUNT, partition, pmem, network, ks);
}

/// Evaluate the stack entries
pub fn evaluate_stack(iterations: usize,
                      partition: &Partition, pmem: &mut PartitionMemory,
                      network: &Network, ks: &KnowledgeSession)
{
    let mut iteration = 0;

    while iteration < iterations {
        iteration += 1;

        let mut owned1_trg_tuples = TupleSets::new();
        let mut owned2_trg_tuples = TupleSets::new();

        if let Some(stack_entry) = pmem.evaluation_stack.pop() {
            let segment = &network.segments[stack_entry.segment_id];
            let smem = &mut pmem.segment_memories[segment.segmentmemory_id];

            let left_tuple_arena = &mut pmem.left_tuples;
            let right_tuple_arena = &mut pmem.right_tuples;

            let mut src_tuples = &mut smem.staged_left_tuples;
            let mut trg_tuples = &mut owned1_trg_tuples; // trg initialised with empty set

            let mut nodes_iter = segment.nodes.iter();
            if let Some(&node_id) = nodes_iter.next() {
                let node = network.betanodes[node_id].get();
                let node_mem = &mut pmem.beta_memories[node.get_betamemory_id().unwrap()];

                // println!("-- {:?}", node_mem);
                // println!("-- {:?}", src_tuples);

                switch_on_betanode(node, node_mem,
                                   src_tuples, trg_tuples,
                                   network, partition, left_tuple_arena, right_tuple_arena,
                                   &ks.fact_store,
                );

                src_tuples.reset_all(); // these are handled
                src_tuples = trg_tuples; // trg becomes the new src
            }
            // println!("output: {:?}", src_tuples);

            trg_tuples = &mut owned2_trg_tuples; // trg initialised with empty set

            for &node_id in nodes_iter {
                let node = network.betanodes[node_id].get();
                let node_mem = &mut pmem.beta_memories[node.get_betamemory_id().unwrap()];

                println!("-- {:?}", node_mem);
                // println!("-- {:?}", src_tuples);

                switch_on_betanode(node, node_mem,
                                   src_tuples, trg_tuples,
                                   network, partition, left_tuple_arena, right_tuple_arena,
                                   &ks.fact_store,
                );

                src_tuples.reset_all(); // these are handled
                std::mem::swap(&mut src_tuples, &mut trg_tuples); // trg becomes the new src, by swapping pointers
            }

            //src_tuples contains the output of this segment
            // println!("output: {:?}", src_tuples);
            if let Some(stack_entry) = pmem.evaluation_stack.last_mut() {
                std::mem::swap(&mut stack_entry.trg_tuples, &mut src_tuples);
            } else {
                // todo: last segment, so this is the rule termination node
            }
        } else {
            // println!("No more entries on stack");
            break;
        }
    }
}
