//! The store module contains the structure where we store all asserted Facts.
use std::collections::{BTreeMap, HashMap};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::ops::{Index, IndexMut};
use std::vec::Vec;

use generational_arena::{Arena, Index as ArenaIndex};
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;

use phreak_facts::FactObject;

use crate::memory::partitionmemory::{LeftTupleId, RightTupleId};
use crate::memory::PartitionMemoryId;

#[derive(Copy, Clone, Debug, Deserialize, Eq, Hash, PartialEq, PartialOrd, Serialize)]
pub struct FactHandle(ArenaIndex);

impl FactHandle {
    #[inline]
    pub fn get_index(&self) -> &ArenaIndex { &self.0 }

    pub fn right_tuples<'a>(&'a self, store: &'a Store) -> impl Iterator<Item=(PartitionMemoryId, RightTupleId)> + 'a
    {
        store
            .get_entry(self)
            .right_tuples
            .iter()
            .flat_map(|(&pid, v)|
                v.iter()
                    .map(move |&x| (pid, x))
            )
    }

    pub fn left_tuples<'a>(&'a self, store: &'a Store) -> impl Iterator<Item=(PartitionMemoryId, LeftTupleId)> + 'a
    {
        store
            .get_entry(self)
            .left_tuples
            .iter()
            .flat_map(|(&pid, v)|
                v.iter()
                    .map(move |&x| (pid, x))
            )
    }
}

impl From<generational_arena::Index> for FactHandle {
    fn from(i: generational_arena::Index) -> Self {
        FactHandle(i)
    }
}

impl From<FactHandle> for generational_arena::Index {
    fn from(h: FactHandle) -> Self {
        h.0
    }
}

impl From<&FactHandle> for generational_arena::Index {
    fn from(h: &FactHandle) -> Self {
        h.0
    }
}

impl Index<FactHandle> for Arena<FactObject> {
    type Output = FactObject;

    fn index(&self, index: FactHandle) -> &Self::Output {
        &self[index.0]
    }
}

impl IndexMut<FactHandle> for Arena<FactObject> {
    fn index_mut(&mut self, index: FactHandle) -> &mut Self::Output {
        &mut self[index.0]
    }
}

struct StoreEntry {
    obj: FactObject,

    left_tuples: HashMap<PartitionMemoryId, Vec<LeftTupleId>>,
    // does this need to be grouped by PartitionId ?
    right_tuples: HashMap<PartitionMemoryId, Vec<RightTupleId>>,
}

impl From<FactObject> for StoreEntry {
    fn from(obj: FactObject) -> Self {
        StoreEntry { obj, left_tuples: HashMap::new(), right_tuples: HashMap::new() }
    }
}

/// Long-time storage of facts
pub struct Store {
    arena: Arena<StoreEntry>,
    handles: BTreeMap<u64, SmallVec<[FactHandle; 1]>>,
}

impl Store {
    /// Create a new Store
    pub fn new() -> Self {
        Store { arena: Arena::new(), handles: BTreeMap::new() }
    }

    pub fn with_capacity(n: usize) -> Self {
        Store { arena: Arena::with_capacity(n), handles: BTreeMap::new() }
    }

    /// Add a single Fact object to the store, and return a Handle to it
    pub fn add_fact(&mut self, fact: FactObject) -> FactHandle {
        let mut s = DefaultHasher::new();
        fact.hash(&mut s);
        let hash = s.finish();

        let handle = self.arena.insert(StoreEntry::from(fact));

        match &mut self.handles.get_mut(&hash) {
            None => {
                let mut v = SmallVec::new();
                v.push(handle.into());
                self.handles.insert(hash, v);
            }
            Some(v) => {
                v.push(handle.into());
            }
        }
        handle.into()
    }

    /// Remove a Fact object by its handle
    pub fn remove_fact(&mut self, handle: FactHandle) -> Option<FactObject> {
        let fact = self.arena.remove(handle.0);
        match &fact {
            None => {}
            Some(f) => {
                let mut s = DefaultHasher::new();
                f.obj.hash(&mut s);
                let hash = s.finish();
                match self.handles.get_mut(&hash) {
                    None => { panic!("Handle not found in Arena: {:?}", handle) }
                    Some(v) => {
                        let i = v.iter().enumerate().find(|h| h.1 == &handle).expect("Handle not found in bucket").0;
                        if v.len() == 1 {
                            v.pop();
                        } else if i == v.len() - 1 {
                            v.remove(i);
                        } else {
                            v.swap_remove(i);
                        }
                    }
                }
                self.handles.remove(&hash);
            }
        }
        fact.map(|f| f.obj)
    }

    /// get the handle to a fact object
    pub fn find_fact(&self, fact: &FactObject) -> Option<FactHandle> {
        let mut s = DefaultHasher::new();
        fact.hash(&mut s);
        let hash = s.finish();

        // get the handle for the given facts object from the bucket
        self.handles.get(&hash).and_then(|bucket| { // get the bucket
            bucket.iter().find(|&index| { // find the entry
                self.arena[index.0].obj == *fact
            }).copied()
        })
    }

    fn get_entry(&self, index: &FactHandle) -> &StoreEntry {
        &self.arena[index.into()]
    }

    fn get_entry_mut(&mut self, index: &FactHandle) -> &StoreEntry {
        &mut self.arena[index.into()]
    }
}

impl Default for Store {
    fn default() -> Self {
        Self::new()
    }
}

impl Index<FactHandle> for Store {
    type Output = FactObject;

    fn index(&self, index: FactHandle) -> &Self::Output {
        &self.arena[index.into()].obj
    }
}

impl Index<&FactHandle> for Store {
    type Output = FactObject;

    fn index(&self, index: &FactHandle) -> &Self::Output {
        &self.arena[index.into()].obj
    }
}
