// -------------------
//    initial values
// -------------------

use crate::network::builder::memory_builder::IndexPrecedenceOption;

/// Initial size of the object store
pub(crate) const START_STORE_SIZE: usize = 16 * 1024;

/// Reserve memory for this amount of nodes.
pub(crate) const START_ALPHANETWORK_SIZE: usize = 1024;

/// Reserve memory for this amount of nodes.
pub(crate) const START_BETANETWORK_SIZE: usize = 1024;

/// Reserve memory for this amount of segments.
pub(crate) const START_SEGMENTS_SIZE: usize = 1024;

/// Reserve memory for this amount of paths.
pub(crate) const START_PATHS_SIZE: usize = 128;

/// Reserve memory for this amount of partitions.
pub(crate) const START_PARTITIONS_SIZE: usize = 8;

/// Initial size of the LeftTuple Arena per partition
pub(crate) const START_LEFTTUPLESTORE_SIZE: usize = 16 * 1024;

/// Initial size of the RightTuple Arena per partition
pub(crate) const START_RIGHTTUPLESTORE_SIZE: usize = 16 * 1024;

/// Number of evaluation iterations before we return control
pub(crate) const ITERATION_COUNT: usize = 1024;

/// Break-off point at which storing inline strings switches to allocating on heap
pub(crate) const SMALL_STRING_SIZE: usize = 10;

// -------------
//    switches
// -------------

pub(crate) const USE_COMPARISON_INDEX: bool = true;
pub(crate) const USE_RANGE_INDEX: bool = USE_COMPARISON_INDEX & false;

/// Struct that holds all dynamic system-wide configuration
#[derive(Debug)]
pub struct GeneralConfig {
    pub composite_key_depth: usize,

    /// True indicates that we will assert objects one by one sequentially, meaning that
    /// objects will only be evaluated on their own, without any relation to other objects.
    /// In this case, we can compile the entire rule set in advance, and we never have to
    /// correlate against previous objects, so we can avoid the memories in the nodes
    pub sequential: bool,

    pub index_precedence_options: IndexPrecedenceOption,
    pub index_left_beta_memory: bool,
    pub index_right_beta_memory: bool,
}

impl GeneralConfig {
    pub fn new() -> Self {
        GeneralConfig {
            composite_key_depth: 3,
            sequential: false,
            index_precedence_options: IndexPrecedenceOption::EqualityPriority,
            index_left_beta_memory: true,
            index_right_beta_memory: true,
        }
    }
}

impl Default for GeneralConfig {
    fn default() -> Self {
        GeneralConfig::new()
    }
}