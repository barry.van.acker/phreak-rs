use crate::network::Network;

pub fn print_alpha_network(network: &Network) {
    let entry_points: Vec<_> = network.alphanodes
        .iter_pairs()
        .filter(|(_, node)| node.parent().is_none())
        .collect();

    for (entry_point_id, node) in entry_points.iter() {
        let entry_point = node.get();
        debug_assert!(entry_point.is_entrynode());

        let mut stack = vec!();

        println!("<ep>");
        println!("{:?}", node.get());
        entry_point_id
            .children(&network.alphanodes)
            .for_each(|child_id| stack.push((0, child_id)));

        while let Some(pair) = stack.pop() {
            let mut indent_str = String::with_capacity(pair.0 * 3);

            let mut i = 0;
            let mut first = ' ';
            while i < pair.0 {
                i += 1;

                indent_str.push(first);
                indent_str.push('-');
                indent_str.push('-');
                first = '-'
            }
            let node = &network.alphanodes[pair.1];
            println!("   {}{:?}", indent_str, node.get());
            pair.1
                .children(&network.alphanodes)
                .for_each(|child_id| stack.push((pair.0 + 1, child_id)));
        }
    }
}

pub fn print_beta_network(network: &Network) {
    let entry_points: Vec<_> = network.betanodes
        .iter_pairs()
        .filter(|(_, node)| node.parent().is_none())
        .collect();

    for (entry_point_id, node) in entry_points.iter() {
        let entry_point = node.get();

        let mut stack = vec!();

        println!("<ep>");
        println!("{:?}", node.get());
        entry_point_id
            .children(&network.betanodes)
            .for_each(|child_id| stack.push((1, child_id)));

        while let Some(pair) = stack.pop() {
            let mut indent_str = String::with_capacity(pair.0 * 3);

            let mut i = 0;
            let mut first = ' ';
            while i < pair.0 {
                i += 1;

                indent_str.push(first);
                indent_str.push('-');
                indent_str.push('-');
                first = '-'
            }
            let node = &network.betanodes[pair.1];
            println!("{}{:?}", indent_str, node.get());
            pair.1
                .children(&network.betanodes)
                .for_each(|child_id| stack.push((pair.0 + 1, child_id)));
        }
    }
}