#![feature(test)]
#![feature(concat_idents)]
#![allow(unused_variables)]

pub mod config;
pub mod store;
pub mod network;
pub mod memory;
pub mod evaluation;
pub mod print;
pub mod prelude;