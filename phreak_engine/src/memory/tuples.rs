use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::{PartitionMemory, RightTupleId};
use crate::network::*;
use crate::network::PhreakBetaNodeId;
use crate::store::{FactHandle, Store};

/// A tuple is an indicator for a partial match.
pub trait Tuple {
    fn get_staged_type(&self) -> StagedType;
    fn set_staged_type(&mut self, staged_type: StagedType);
    fn clear_staged(&mut self);

    fn get_fact_handle(&self) -> FactHandle;
}

/// Stage a tuple for processing in a node.
#[derive(Copy, Clone, Deserialize, Serialize, PartialEq)]
pub enum StagedType {
    None,
    Insert,
    Update,
    Delete,
    NormalizedDelete,
}

/// A right tuple represents the fact that an Object has passed all static tests on
/// its attributes (tested in the Alpha network)
///
#[derive(Deserialize, Serialize)]
pub struct RightTuple {
    pub(crate) fact_handle: FactHandle,
    pub(crate) staged_type: StagedType,
    pub(crate) expired: bool,
    pub(crate) owner: PhreakBetaNodeId,
    pub(crate) retracted: bool,
}

impl RightTuple {
    pub fn new(fact_handle: FactHandle, owner: PhreakBetaNodeId) -> Self {
        RightTuple {
            fact_handle,
            staged_type: StagedType::None,
            expired: false,
            owner,
            retracted: false,
        }
    }

    pub fn retract_tuple(pid: PartitionId,
                         rid: RightTupleId,
                         network: &Network,
                         store: &Store,
                         pm: &mut PartitionMemory,
    ) {
        todo!()
        // let rt = ks.partition_memories[pid].get_right_tuple_mut();
        //
        // let nid = None;
        // if !rt.retracted.swap(true, Ordering::Relaxed) {
        //     nid = Some(rt.owner);
        //
        // }
        //
        // if let Some(nid) = nid {
        //
        //     retract_right_tuple(pid, nid, rid, network, ks)
        // }
    }
}

impl Tuple for RightTuple {
    fn get_staged_type(&self) -> StagedType { self.staged_type }
    fn set_staged_type(&mut self, staged_type: StagedType) { self.staged_type = staged_type; }
    fn clear_staged(&mut self) { self.staged_type = StagedType::None; }

    fn get_fact_handle(&self) -> FactHandle { self.fact_handle }
}

/// A left tuple indicates a partial match of an Object. Each LeftTuple indicates that a
/// certain prefix of the tests matches the Object, and a tuple in the TerminalNode
/// represents the full set of successful tests.
///
/// A tuple can be for a staged Insert, Update or Delete, or for an asserted state in a node.
#[derive(Deserialize, Serialize)]
pub struct LeftTuple {
    pub(crate) staged_type: StagedType,
    pub(crate) expired: bool,
    pub(crate) right_parent: Option<RightTupleId>,
    pub(crate) fact_handle: FactHandle,
}

impl LeftTuple {
    pub fn new(fact_handle: FactHandle, right_parent: Option<RightTupleId>) -> Self {
        LeftTuple {
            staged_type: StagedType::None,
            expired: false,
            right_parent,
            fact_handle: fact_handle,
        }
    }
}

impl Tuple for LeftTuple {
    fn get_staged_type(&self) -> StagedType { self.staged_type }
    fn set_staged_type(&mut self, staged_type: StagedType) { self.staged_type = staged_type; }
    fn clear_staged(&mut self) { self.staged_type = StagedType::None; }

    fn get_fact_handle(&self) -> FactHandle { self.fact_handle }
}
