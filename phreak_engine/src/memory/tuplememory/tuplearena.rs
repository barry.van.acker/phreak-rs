use generational_arena::{Arena, Index};
use generational_indextree::{Arena as TreeArena, NodeId as TreeIndex};

/// Wrapper type that maps arena methods to uniform named methods
pub trait TupleArena<TID, T> {
    fn arena_get(&self, id: TID) -> &T;
}

/// Wrapper type that maps mutable arena methods to uniform named methods
pub trait TupleArenaMut<TID, T> {
    fn arena_get_mut(&mut self, id: TID) -> &mut T;
}

impl<T> TupleArena<TreeIndex, T> for TreeArena<T> {
    #[inline]
    fn arena_get(&self, id: TreeIndex) -> &T { self[id].get() }
}

impl<T> TupleArenaMut<TreeIndex, T> for TreeArena<T> {
    #[inline]
    fn arena_get_mut(&mut self, id: TreeIndex) -> &mut T { self[id].get_mut() }
}

impl<T> TupleArena<Index, T> for Arena<T> {
    #[inline]
    fn arena_get(&self, id: Index) -> &T { &self[id] }
}

impl<T> TupleArenaMut<Index, T> for Arena<T> {
    #[inline]
    fn arena_get_mut(&mut self, id: Index) -> &mut T { &mut self[id] }
}

/// Read-only wrapper
pub enum TupleArenaWrapper<'a, T> {
    Arena(&'a Arena<T>),
    TreeArena(&'a TreeArena<T>),
}

impl<'a, T> From<&'a Arena<T>> for TupleArenaWrapper<'a, T> {
    fn from(a: &'a Arena<T>) -> Self {
        TupleArenaWrapper::Arena(a)
    }
}

impl<'a, T> From<&'a TreeArena<T>> for TupleArenaWrapper<'a, T> {
    fn from(a: &'a TreeArena<T>) -> Self {
        TupleArenaWrapper::TreeArena(a)
    }
}

impl<'a, T> TupleArena<TreeIndex, T> for TupleArenaWrapper<'a, T> {
    #[inline]
    fn arena_get(&self, id: TreeIndex) -> &T {
        match self {
            TupleArenaWrapper::Arena(_) => { panic!() }
            TupleArenaWrapper::TreeArena(a) => { &a[id].get() }
        }
    }
}

impl<'a, T> TupleArena<Index, T> for TupleArenaWrapper<'a, T> {
    #[inline]
    fn arena_get(&self, id: Index) -> &T {
        match self {
            TupleArenaWrapper::Arena(a) => { &a[id] }
            TupleArenaWrapper::TreeArena(_) => { panic!() }
        }
    }
}

/// Mutable wrapper
pub enum TupleArenaMutWrapper<'a, T> {
    Arena(&'a mut Arena<T>),
    TreeArena(&'a mut TreeArena<T>),
}

impl<'a, T> From<&'a mut Arena<T>> for TupleArenaMutWrapper<'a, T> {
    fn from(a: &'a mut Arena<T>) -> Self {
        TupleArenaMutWrapper::Arena(a)
    }
}

impl<'a, T> From<&'a mut TreeArena<T>> for TupleArenaMutWrapper<'a, T> {
    fn from(a: &'a mut TreeArena<T>) -> Self {
        TupleArenaMutWrapper::TreeArena(a)
    }
}

impl<'a, T> TupleArena<TreeIndex, T> for TupleArenaMutWrapper<'a, T> {
    #[inline]
    fn arena_get(&self, id: TreeIndex) -> &T {
        match self {
            TupleArenaMutWrapper::Arena(_) => { panic!() }
            TupleArenaMutWrapper::TreeArena(a) => { a[id].get() }
        }
    }
}

impl<'a, T> TupleArenaMut<TreeIndex, T> for TupleArenaMutWrapper<'a, T> {
    #[inline]
    fn arena_get_mut(&mut self, id: TreeIndex) -> &mut T {
        match self {
            TupleArenaMutWrapper::Arena(_) => { panic!() }
            TupleArenaMutWrapper::TreeArena(a) => { a[id].get_mut() }
        }
    }
}

impl<'a, T> TupleArena<Index, T> for TupleArenaMutWrapper<'a, T> {
    #[inline]
    fn arena_get(&self, id: Index) -> &T {
        match self {
            TupleArenaMutWrapper::Arena(a) => { &a[id] }
            TupleArenaMutWrapper::TreeArena(_) => { panic!() }
        }
    }
}

impl<'a, T> TupleArenaMut<Index, T> for TupleArenaMutWrapper<'a, T> {
    #[inline]
    fn arena_get_mut(&mut self, id: Index) -> &mut T {
        match self {
            TupleArenaMutWrapper::Arena(a) => { &mut a[id] }
            TupleArenaMutWrapper::TreeArena(_) => { panic!() }
        }
    }
}

