use std::fmt::{Debug, Formatter};

use serde::{Deserialize, Serialize};

use crate::memory::tuplememory::{TupleIndexType, TupleMemory};
use crate::memory::tuplememory::tuplearena::TupleArena;
use crate::memory::tuples::Tuple;
use crate::store::Store;

/// A TupleList memory is used when no optimisation is offered for the comparison operator in
/// the node.
#[derive(Deserialize, Serialize)]
pub struct TupleListMemory<TID, T, A>
    where
        TID: Copy + PartialEq,
        T: Tuple,
        A: TupleArena<TID, T>
{
    data: Vec<TID>,
    _mark: std::marker::PhantomData<(T, A)>,
}

impl<TID, T, A> TupleListMemory<TID, T, A>
    where
        TID: Copy + PartialEq,
        T: Tuple,
        A: TupleArena<TID, T>
{
    pub fn new() -> Self {
        TupleListMemory { data: vec![], _mark: std::marker::PhantomData {} }
    }

    pub fn reserve(&mut self, size: usize) { self.data.reserve(size); }
    pub fn is_empty(&self) -> bool { self.data.is_empty() }

    pub fn iter(&self) -> Iter<TID> {
        Iter { inner: self.data.iter() }
    }
}

impl<TID, T, A> Default for TupleListMemory<TID, T, A>
    where
        TID: Copy + PartialEq,
        T: Tuple,
        A: TupleArena<TID, T>
{
    fn default() -> Self {
        Self::new()
    }
}

impl<TID, T, A> Debug for TupleListMemory<TID, T, A>
    where
        TID: Copy + PartialEq,
        T: Tuple,
        A: TupleArena<TID, T>
{
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "<TupleListMemory len={}>", self.data.len())?;
        Ok(())
    }
}

impl<TID, T, A> TupleMemory<TID, T, A>
for TupleListMemory<TID, T, A>
    where
        TID: Copy + PartialEq,
        T: Tuple,
        A: TupleArena<TID, T>
{
    fn add(&mut self, tuple_id: TID, _tuple_arena: &A, _s: &Store) {
        let r = self.data.push(tuple_id);
    }

    fn remove(&mut self, tuple_id: TID, _tuple_arena: &A, _s: &Store) {
        self.data.remove(self.data.iter().position(|&id| id == tuple_id).unwrap());
    }

    fn contains(&mut self, tuple_id: TID, _tuple_arena: &A, _s: &Store) -> bool {
        self.data.contains(&tuple_id)
    }

    fn remove_add(&mut self, tuple_id: TID, tuple_arena: &A, s: &Store) {
        self.remove(tuple_id, tuple_arena, s);
        self.add(tuple_id, tuple_arena, s);
    }

    fn len(&self) -> usize {
        self.data.len()
    }

    fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    fn clear(&mut self) {
        self.data.clear()
    }

    fn is_indexed(&self) -> bool {
        false
    }

    fn get_index_type(&self) -> TupleIndexType {
        TupleIndexType::None
    }
}

pub struct Iter<'a, TID>
    where
        TID: Copy + PartialEq
{
    inner: std::slice::Iter<'a, TID>,
}

impl<'a, TID> Iterator for Iter<'a, TID>
    where
        TID: Copy + PartialEq
{
    type Item = TID;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().copied()
    }
}