use rbtree::RBTree;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

use crate::memory::tuplememory::TupleArena;

/// TupleRBTree memory is used when we optimize for comparison based conditions in the
/// node.

/// Store ID and reference to store S
#[derive(Deserialize, Serialize)]
pub struct RBWrapper<'s, TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
(
    TID,
    &'s S,
    std::marker::PhantomData<T>,
);

impl<'s, TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
Ord for RBWrapper<'s, TID, T, S> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.1.arena_get(self.0).cmp(other.1.arena_get(other.0))
    }
}

impl<'s TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
PartialOrd for RBWrapper<'s, TID, T, S> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.1.arena_get(self.0).partial_cmp(other.1.arena_get(other.0))
    }
}

impl<'s, TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
Eq for RBWrapper<'s, TID, T, S> {}

impl<TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
PartialEq for RBWrapper<'s, TID, T, S> {
    fn eq(&self, other: &Self) -> bool {
        self.1.arena_get(self.0) == other.1.arena_get(other.0)
    }
}

pub struct TupleRBTreeMemory<'s, TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>>
(
    RBTree<RBWrapper<'s, TID, T, S>, ()>,
    std::marker::PhantomData<T>,
);


impl<TID: Copy, T: PartialOrd + Ord + PartialEq, S: TupleArena<TID, T>> Debug for TupleRBTreeMemory<TID, T, S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "<TupleRBTreeMemory len={}>", self.0.len())?;
        Ok(())
    }
}
