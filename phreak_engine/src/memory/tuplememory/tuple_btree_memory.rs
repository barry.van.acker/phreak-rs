use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::fmt::{Debug, Formatter};

use crate::memory::tuplememory::{TupleIndexType, TupleMemory};

/// A TupleList memory is used when optimisation is offered for the equality comparison operator in
/// the node.
#[derive(Deserialize, Serialize)]
pub struct TupleBtreeMemory<T, TID: std::cmp::Ord> (
    BTreeSet<TID>,
    std::marker::PhantomData<T>,
);

impl<T, TID: std::cmp::Ord + Copy> TupleMemory<T, TID>
for TupleBtreeMemory<T, TID>
{
    fn add(&mut self, tuple_id: TID) {
        let r = self.0.insert(tuple_id);
        debug_assert!(r, true);
    }

    fn remove(&mut self, tuple_id: TID) {
        let r = self.0.remove(&tuple_id);
        debug_assert!(r, true);
    }

    fn contains(&mut self, tuple_id: TID) -> bool {
        self.0.contains(&tuple_id)
    }

    fn remove_add(&mut self, tuple_id: TID) {
        self.remove(tuple_id);
        self.add(tuple_id);
    }

    fn len(&self) -> usize {
        self.0.len()
    }

    fn is_empty(&self) -> bool { self.0.is_empty() }

    fn clear(&mut self) {
        self.0.clear()
    }

    fn is_indexed(&self) -> bool {
        true
    }

    fn get_index_type(&self) -> TupleIndexType {
        TupleIndexType::Equal
    }
}

impl<T, TID: std::cmp::Ord> Debug for TupleBtreeMemory<T, TID> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "<TupleBtreeMemory len={}>", self.0.len())?;
        Ok(())
    }
}
