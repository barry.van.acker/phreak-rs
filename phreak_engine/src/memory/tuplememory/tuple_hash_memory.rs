use std::collections::HashMap;
use std::convert::From;
use std::fmt::{Debug, Formatter};

use generational_arena::Arena;
use generational_indextree::Arena as TreeArena;
use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::{LeftTupleId, RightTupleId};
use crate::memory::tuplememory::{TupleIndexType, TupleMemory};
use crate::memory::tuplememory::tuplearena::{TupleArena, TupleArenaWrapper};
use crate::memory::tuples::{LeftTuple, RightTuple, Tuple};
use crate::network::builder::memory_builder::{IndexKey, IndexSpec};
use crate::store::Store;

macro_rules! tuplehash_memory {
    ($NAME:ident, $ITER:ident, $TID:ty, $T:ty, $A:ty) => {

        #[derive(Deserialize, Serialize)]
        pub struct $NAME {
            data: HashMap<IndexKey, $TID>,
            index_spec: IndexSpec,
        }

        impl $NAME {
            pub fn new(spec: &IndexSpec) -> Self {
                $NAME {
                    data: HashMap::new(),
                    index_spec: spec.clone(),
                }
            }

            pub fn reserve(&mut self, size: usize) {
                self.data.reserve(size)
            }

            pub fn iter(&self) -> $ITER {
                $ITER{ inner: self.data.iter() }
            }
        }

        impl Debug for $NAME {
            fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
                write!(f, "<TupleHashMemory len={}>", self.data.len())?;
                Ok(())
            }
        }

        impl TupleMemory<$TID, $T, $A> for $NAME
        {
            fn add(&mut self, tuple_id: $TID, tuple_arena: &$A, s: &Store) {
                let w = TupleArenaWrapper::from(tuple_arena);
                let t = w.arena_get(tuple_id);
                let o = &s[t.get_fact_handle()];

                let index = IndexKey::new(&self.index_spec, t.get_fact_handle(), o);

                let r = self.data.insert(index, tuple_id);
            }

            fn remove(&mut self, tuple_id: $TID, tuple_arena: &$A, s: &Store) {
                let w = TupleArenaWrapper::from(tuple_arena);
                let t = w.arena_get(tuple_id);
                let o = &s[t.get_fact_handle()];

                let index = IndexKey::new(&self.index_spec, t.get_fact_handle(), o);

                self.data.remove(&index);
            }

            fn contains(&mut self, tuple_id: $TID, tuple_arena: &$A, s: &Store) -> bool {
                let w = TupleArenaWrapper::from(tuple_arena);
                let t = w.arena_get(tuple_id);
                let o = &s[t.get_fact_handle()];

                let index = IndexKey::new(&self.index_spec, t.get_fact_handle(), o);

                self.data.get(&index).map_or(false, |tid| *tid == tuple_id )
            }

            fn remove_add(&mut self, tuple_id: $TID, tuple_arena: &$A, s: &Store) {
                self.remove(tuple_id, tuple_arena, s);
                self.add(tuple_id, tuple_arena, s);
            }

            fn len(&self) -> usize {
                self.data.len()
            }

            fn is_empty(&self) -> bool {
                self.data.is_empty()
            }

            fn clear(&mut self) {
                self.data.clear()
            }

            fn is_indexed(&self) -> bool {
                true
            }

            fn get_index_type(&self) -> TupleIndexType {
                TupleIndexType::Equal
            }
        }

        pub struct $ITER<'a> {
            inner:  std::collections::hash_map::Iter<'a, IndexKey, $TID>
        }

        impl<'a> Iterator for $ITER<'a> {
            type Item = (&'a IndexKey, &'a $TID);

            fn next(&mut self) -> Option<Self::Item> {
                self.inner.next()
            }

        }

    }
}

tuplehash_memory!( LeftTupleHashMemory, LeftTupleHashMemoryIter, LeftTupleId, LeftTuple, TreeArena<LeftTuple> );
tuplehash_memory!( RightTupleHashMemory, RightTupleHashMemoryIter, RightTupleId, RightTuple, Arena<RightTuple> );
