pub(crate) use tuplememory::{LeftTupleMemoryEnum, RightTupleMemoryEnum};

use crate::memory::tuples::Tuple;
use crate::store::Store;

pub enum TupleIndexType {
    None,
    Equal,
    Comparison,
    Range,
}

impl TupleIndexType {
    pub fn is_comparison(&self) -> bool {
        match self {
            TupleIndexType::None => false,
            TupleIndexType::Equal => false,
            TupleIndexType::Comparison => true,
            TupleIndexType::Range => true,
        }
    }
}

/// Generic trait for all tuple memories.
pub trait TupleMemory<TID, T, A>
    where
        TID: Copy,
        T: Tuple
{
    fn add(&mut self, tuple_id: TID, tuple_arena: &A, s: &Store);
    fn remove(&mut self, tuple_id: TID, tuple_arena: &A, s: &Store);
    fn contains(&mut self, tuple_id: TID, tuple_arena: &A, s: &Store) -> bool;
    fn remove_add(&mut self, tuple_id: TID, tuple_arena: &A, s: &Store);

    fn len(&self) -> usize;
    fn is_empty(&self) -> bool;
    fn clear(&mut self);

    fn is_indexed(&self) -> bool;
    fn get_index_type(&self) -> TupleIndexType;
}

pub mod tuplearena;
pub mod tuplememory;
pub mod tuple_list_memory;
pub mod tuple_hash_memory;
// pub mod tuple_btree_memory;
// pub mod tuple_rbtree_memory;
