use std::fmt::{Debug, Formatter};

use generational_arena::Arena;
use generational_indextree::Arena as TreeArena;
use serde::{Deserialize, Serialize};

use crate::memory::partitionmemory::{LeftTupleId, RightTupleId};
use crate::memory::tuplememory::{TupleIndexType, TupleMemory};
use crate::memory::tuplememory::tuple_hash_memory::*;
use crate::memory::tuplememory::tuple_list_memory::{Iter as TupleListIter, TupleListMemory};
use crate::memory::tuples::{LeftTuple, RightTuple};
use crate::store::Store;

/// Creates either a Left or Right version of a TupleMemoryEnum
///
/// The type system doesn't like us providing a TreeArena for LeftTuples and an Arena for
/// RightTuples, with all derived types, and wrapped references with lifetimes, so
/// instead we use the macro system to generate exactly one implementation per Left and Right.
///
/// This implements an enumeration containing a memory to store references to tuples in the
/// partionmemory arenas. We can optimize the storage by selecting a different algorithm tailored
/// to the type of lookup we will be doing.
///
macro_rules! tuplememory_enum {
    ($NAME:ident, $ITER:ident, $TID:ty, $T:ty, $A:ty, $THM:ty, $THMI:ty ) => {

        /// Instead of dynamic dispatch, we will use an enum to switch between these implementations
        #[derive(Deserialize, Serialize)]
        pub(crate) enum $NAME
        {
            /// fastest, but might not provide enough guarantees
            None,

            /// provides sequential ordering, with linear search
            TupleList(TupleListMemory<$TID, $T, $A>),

            /// hash based on a single index
            TupleHash($THM),

            // provides comparison
            //TupleBtree(TupleBtreeMemory<TID, T>),

        //    TupleRBTree,
        }

        impl Debug for $NAME
        {
            fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
                match self {
                    $NAME::None =>
                        write!(f, "<TupleMemoryEnum::None>")?,
                    $NAME::TupleList(t) =>
                        write!(f, "<TupleMemoryEnum::TupleList {:?}>", t)?,
                    $NAME::TupleHash(t) =>
                        write!(f, "<TupleMemoryEnum::TupleHash {:?}>", t)?,
                }
                Ok(())
            }
        }

        impl TupleMemory<$TID, $T, $A> for $NAME
        {
            fn add(&mut self, tuple_id: $TID, tuple_arena: & $A, s: &Store) {
                match self {
                    $NAME::None => {}
                    $NAME::TupleList(t) =>
                        { t.add(tuple_id, tuple_arena, s) }
                    $NAME::TupleHash(t) =>
                        { t.add(tuple_id, tuple_arena, s) }
                }
            }

            fn remove(&mut self, tuple_id: $TID, tuple_arena: & $A, s: &Store) {
                match self {
                    $NAME::None => {}
                    $NAME::TupleList(t) =>
                        { t.remove(tuple_id, tuple_arena, s) }
                    $NAME::TupleHash(t) =>
                        { t.remove(tuple_id, tuple_arena, s) }
                }
            }

            fn contains(&mut self, tuple_id: $TID, tuple_arena: & $A, s: &Store) -> bool {
                match self {
                    $NAME::None => { false }
                    $NAME::TupleList(t) =>
                        { t.contains(tuple_id, tuple_arena, s) }
                    $NAME::TupleHash(t) =>
                        { t.contains(tuple_id, tuple_arena, s) }
                }
            }

            fn remove_add(&mut self, tuple_id: $TID, tuple_arena: & $A, s: &Store) {
                match self {
                    $NAME::None => {}
                    $NAME::TupleList(t) =>
                        { t.remove_add(tuple_id, tuple_arena, s) }
                    $NAME::TupleHash(t) =>
                        { t.remove_add(tuple_id, tuple_arena, s) }
                }
            }

            fn len(&self) -> usize {
                match self {
                    $NAME::None =>
                        { 0 }
                    $NAME::TupleList(t) =>
                        { t.len() }
                    $NAME::TupleHash(t) =>
                        { t.len() }
                }
            }

            fn is_empty(&self) -> bool {
                match self {
                    $NAME::None =>
                        { true }
                    $NAME::TupleList(t) =>
                        { t.is_empty() }
                    $NAME::TupleHash(t) =>
                        { t.is_empty() }
                }
            }

            fn clear(&mut self) {
                match self {
                    $NAME::None => {}
                    $NAME::TupleList(t) =>
                        { t.clear() }
                    $NAME::TupleHash(t) =>
                        { t.clear() }
                }
            }

            fn is_indexed(&self) -> bool {
                match self {
                    $NAME::None =>
                        { false }
                    $NAME::TupleList(t) =>
                        { t.is_indexed() }
                    $NAME::TupleHash(t) =>
                        { t.is_indexed() }
                }
            }

            fn get_index_type(&self) -> TupleIndexType {
                match self {
                    $NAME::None =>
                        { TupleIndexType::None }
                    $NAME::TupleList(t) =>
                        { t.get_index_type() }
                    $NAME::TupleHash(t) =>
                        { t.get_index_type() }
                }
            }
        }


        impl $NAME {
            fn get_impl_type_name(&self) -> &str {
                match self {
                    $NAME::None =>
                        { "None" }
                    $NAME::TupleList(_) =>
                        { "TupleList" }
                    $NAME::TupleHash(_) =>
                        { "TupleHash" }
                }
            }

            /// Reserve a block of data large enough to hold the upcoming batch of 'size' items
            pub(crate) fn reserve(&mut self, size: usize) {
                match self {
                    $NAME::None => {}
                    $NAME::TupleList(t) =>
                        { t.reserve(size) }
                    $NAME::TupleHash(t) =>
                        { t.reserve(size) }
                }
            }

            /// Iterate the items in the list in unspecified order
            pub(crate) fn iter(&self) -> $ITER {
                match self {
                    $NAME::None =>
                        { $ITER::None }
                    $NAME::TupleList(t) =>
                        { $ITER::TupleList(Some(t.iter())) }
                    $NAME::TupleHash(t) =>
                        { $ITER::TupleHash(Some(t.iter()) ) }
                }
            }
        }

        pub(crate) enum $ITER <'a>
        {
            None,
            TupleList(Option<TupleListIter<'a, $TID>>),
            TupleHash(Option<$THMI>),
        }

        impl<'a> Iterator for $ITER <'a>
        {
            type Item = $TID;

            fn next(&mut self) -> Option<Self::Item> {
                match self {
                    $ITER::None => None,
                    $ITER::TupleList(inner) => {
                        match inner {
                            None => None,
                            Some(t) => t.next()
                        }
                    }
                    $ITER::TupleHash(inner) => {
                        match inner {
                            None => None,
                            Some(t) => t.next().map(|(h,&tid)|tid)
                        }
                    }
                }
            }
        }

    }
}

tuplememory_enum!(
    LeftTupleMemoryEnum, LeftTupleMemoryEnumIter,
    LeftTupleId, LeftTuple, TreeArena<LeftTuple>,
    LeftTupleHashMemory, LeftTupleHashMemoryIter<'a>
    );
tuplememory_enum!(
    RightTupleMemoryEnum, RightTupleMemoryEnumIter,
    RightTupleId, RightTuple, Arena<RightTuple>,
    RightTupleHashMemory, RightTupleHashMemoryIter<'a>
    );
