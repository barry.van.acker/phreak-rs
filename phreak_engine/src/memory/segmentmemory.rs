use serde::{Deserialize, Serialize};

use crate::memory::collections::tuplesets::TupleSets;
use crate::memory::partitionmemory::LeftTupleId;
use crate::memory::tuples::LeftTuple;

/// Shared Memory for a segment of beta nodes
#[derive(Deserialize, Serialize)]
pub struct SegmentMemory {
    pub(crate) staged_left_tuples: TupleSets<LeftTuple, LeftTupleId>,
}

impl SegmentMemory {
    pub fn new() -> Self {
        SegmentMemory {
            staged_left_tuples: TupleSets::new(),
        }
    }

}

impl Default for SegmentMemory {
    fn default() -> Self {
        Self::new()
    }
}
