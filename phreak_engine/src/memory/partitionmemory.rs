use generational_arena::{Arena, Index as ArenaIndex};
use generational_indextree::{Arena as TreeArena, NodeId as TreeIndex};

use crate::config::*;
use crate::evaluation::StackEntry;
use crate::memory::betamemory::BetaMemory;
use crate::memory::pathmemory::PathMemory;
use crate::memory::segmentmemory::SegmentMemory;
use crate::memory::tuples::{LeftTuple, RightTuple};
use crate::network::PhreakBetaNodeId;
use crate::store::FactHandle;

pub type RightTupleId = ArenaIndex;
pub type LeftTupleId = TreeIndex;

/// Memory shared by all nodes in a partition of the network. Multiple partitions can be
/// evaluated in parallel.
pub struct PartitionMemory {
    pub(crate) right_tuples: Arena<RightTuple>,
    pub(crate) left_tuples: TreeArena<LeftTuple>,

    pub(crate) beta_memories: Arena<BetaMemory>,
    pub(crate) segment_memories: Arena<SegmentMemory>,
    pub(crate) path_memories: Arena<PathMemory>,

    pub(crate) evaluation_stack: Vec<StackEntry>,
}

impl PartitionMemory {
    pub fn new() -> Self {
        PartitionMemory {
            right_tuples: Arena::with_capacity(START_RIGHTTUPLESTORE_SIZE),
            left_tuples: TreeArena::with_capacity(START_LEFTTUPLESTORE_SIZE),
            beta_memories: Arena::with_capacity(START_BETANETWORK_SIZE),
            segment_memories: Arena::with_capacity(START_SEGMENTS_SIZE),
            path_memories: Arena::with_capacity(START_PATHS_SIZE),
            evaluation_stack: Vec::new(),
        }
    }

    pub fn create_right_tuple(
        right_tuples: &mut Arena<RightTuple>,
        fact_handle: FactHandle,
        owner: PhreakBetaNodeId,
    )
        -> (RightTupleId, &mut RightTuple)
    {
        let id = right_tuples.insert(
            RightTuple::new(fact_handle, owner));

        (id, &mut right_tuples[id])
    }

    pub fn create_left_tuple(
        handle: FactHandle,
        left_tuples: &mut TreeArena<LeftTuple>,
    )
        -> (LeftTupleId, &mut LeftTuple)
    {
        let id = left_tuples.new_node(
            LeftTuple::new(handle, None));

        (id, left_tuples[id].get_mut())
    }

    pub fn get_right_tuple(&self, id: RightTupleId) -> &RightTuple {
        &self.right_tuples[id]
    }

    pub fn get_right_tuple_mut(&mut self, id: RightTupleId) -> &mut RightTuple {
        &mut self.right_tuples[id]
    }

    pub fn get_left_tuple(&self, id: LeftTupleId) -> &LeftTuple {
        self.left_tuples[id].get()
    }

    pub fn get_left_tuple_mut(&mut self, id: LeftTupleId) -> &mut LeftTuple {
        self.left_tuples[id].get_mut()
    }
}

impl Default for PartitionMemory {
    fn default() -> Self {
        Self::new()
    }
}
