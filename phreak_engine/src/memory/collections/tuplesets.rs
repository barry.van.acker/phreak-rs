use std::fmt::{Debug, Result};
use std::marker::PhantomData;
use std::mem::swap;

use serde::{Deserialize, Serialize};
use smallvec::alloc::collections::VecDeque;
use smallvec::alloc::fmt::Formatter;

use crate::memory::tuples::{StagedType, Tuple};
use crate::memory::tuples::StagedType::*;
use std::collections::vec_deque::Iter;

/// Tuplesets of either LeftTuples or RightTuples are used to store pending
/// inserts, updates and deletes for a BetaNode
#[derive(Deserialize, Serialize)]
pub struct TupleSets<T: Tuple, ID: Copy + std::cmp::PartialEq> {
    inserts: VecDeque<ID>,
    // todo: more efficient store, like linked list?
    updates: VecDeque<ID>,
    deletes: VecDeque<ID>,
    normalized_deletes: VecDeque<ID>,

    _tuple: std::marker::PhantomData<T>,
}

impl<T: Tuple, ID: Copy + std::cmp::PartialEq> TupleSets<T, ID> {
    pub fn new() -> Self {
        TupleSets {
            inserts: VecDeque::new(),
            updates: VecDeque::new(),
            deletes: VecDeque::new(),
            normalized_deletes: VecDeque::new(),
            _tuple: PhantomData,
        }
    }
    /// schedule a new Insert Tuple id, and return true iff the Insert list was empty
    pub fn add_insert(&mut self, id: ID, t: &mut T) -> bool {
        if let Update = t.get_staged_type() {return false; }

        t.set_staged_type(Insert);
        let r = self.inserts.is_empty();
        self.inserts.push_back(id);
        r
    }

    /// schedule a new Update Tuple id, and return true iff the Update list was empty
    pub fn add_update(&mut self, id: ID, t: &mut T) -> bool {
        match t.get_staged_type() {
            StagedType::None => {}
            _ => { return false; }
        }
        t.set_staged_type(Update);
        let r = self.updates.is_empty();
        self.updates.push_back(id);
        r
    }

    /// schedule a new Delete Tuple id, and return true iff the Delete list was empty
    pub fn add_delete(&mut self, id: ID, t: &mut T) -> bool {
        match t.get_staged_type() {
            Insert => {
                self.remove_insert(id, t);
                return self.deletes.is_empty();
            }
            Update => {
                self.remove_update(id, t);
            }
            _ => {}
        }
        t.set_staged_type(Update);
        let r = self.deletes.is_empty();
        self.deletes.push_back(id);
        r
    }

    /// schedule a new NormalizedDelete Tuple id, and return true iff the NormalizedDelete list was empty
    pub fn add_normalized_delete(&mut self, id: ID, t: &mut T) -> bool {
        t.set_staged_type(NormalizedDelete);
        let r = self.normalized_deletes.is_empty();
        self.normalized_deletes.push_back(id);
        r
    }

    pub fn remove_insert(&mut self, id: ID, t: &mut T) {
        t.clear_staged();
        self.inserts.remove(
            self.inserts
                .iter()
                .position(|&item| item == id)
                .unwrap()
        );
    }
    pub fn remove_update(&mut self, id: ID, t: &mut T) {
        t.clear_staged();
        self.updates.remove(
            self.updates
                .iter()
                .position(|&item| item == id)
                .unwrap()
        );
    }
    pub fn remove_delete(&mut self, id: ID, t: &mut T) {
        t.clear_staged();
        self.deletes.remove(
            self.deletes
                .iter()
                .position(|&item| item == id)
                .unwrap()
        );
    }
    pub fn remove_normalized_delete(&mut self, _id: ID, _t: &mut T) { unimplemented!() }

    pub fn reset_all(&mut self) {
        self.inserts.clear();
        self.updates.clear();
        self.deletes.clear();
        self.normalized_deletes.clear();
    }

    pub fn get_insert_size(&self) -> usize { self.inserts.len() }
    pub fn get_update_size(&self) -> usize { self.updates.len() }
    pub fn get_delete_size(&self) -> usize { self.deletes.len() }
    pub fn get_normalized_delete_size(&self) -> usize { self.normalized_deletes.len() }

    pub fn get_inserts(&self) -> (&[ID], &[ID]) { self.inserts.as_slices() }
    pub fn get_updates(&self) -> (&[ID], &[ID]) { self.updates.as_slices() }
    pub fn get_deletes(&self) -> (&[ID], &[ID]) { self.deletes.as_slices() }
    pub fn get_normalized_deletes(&self) -> (&[ID], &[ID]) { self.normalized_deletes.as_slices() }

    pub fn insert_is_empty(&self) -> bool { self.inserts.is_empty() }
    pub fn update_is_empty(&self) -> bool { self.updates.is_empty() }
    pub fn delete_is_empty(&self) -> bool { self.deletes.is_empty() }
    pub fn normalized_delete_is_empty(&self) -> bool { self.normalized_deletes.is_empty() }

    pub fn iter_inserts(&self) -> Iter<'_, ID> { self.inserts.iter() }
    pub fn iter_updates(&self) -> Iter<'_, ID> { self.updates.iter() }
    pub fn iter_deletes(&self) -> Iter<'_, ID> { self.deletes.iter() }
    pub fn iter_normalized_deletes(&self) -> Iter<'_, ID> { self.normalized_deletes.iter() }

    pub fn add_all_inserts(&mut self, other: &mut Self) {
        if self.inserts.is_empty() {
            swap(&mut self.inserts, &mut other.inserts);
        } else {
            self.inserts.append(&mut other.inserts);
        }
    }

    pub fn add_all_updates(&mut self, other: &mut Self) {
        if self.updates.is_empty() {
            swap(&mut self.updates, &mut other.updates);
        } else {
            self.updates.append(&mut other.updates);
        }
    }

    pub fn add_all_deletes(&mut self, other: &mut Self) {
        if self.deletes.is_empty() {
            swap(&mut self.deletes, &mut other.deletes);
        } else {
            self.deletes.append(&mut other.deletes);
        }
    }

    pub fn add_all(&mut self, other: &mut Self) {
        self.add_all_inserts(other);
        self.add_all_updates(other);
        self.add_all_deletes(other);
    }

    pub fn is_empty(&self) -> bool {
        self.inserts.is_empty() &&
            self.updates.is_empty() &&
            self.deletes.is_empty() &&
            self.normalized_deletes.is_empty()
    }
}

impl<T: Tuple, ID: Copy + std::cmp::PartialEq> Debug for TupleSets<T, ID> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "TupleSets hasInserts={} hasUpdates={} hasDeletes={} hasNormalizedDeletes={}",
               self.inserts.len(),
               self.updates.len(),
               self.deletes.len(),
               self.normalized_deletes.len())
    }
}

impl<T: Tuple, ID: Copy + std::cmp::PartialEq> Default for TupleSets<T, ID> {
    fn default() -> Self {
        Self::new()
    }
}
