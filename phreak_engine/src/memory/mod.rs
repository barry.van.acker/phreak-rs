//! The memory module contains data structures related to tracking the state of
//! (partial) matches.

use std::sync::Mutex;

use generational_arena::{Arena, Index as ArenaIndex};

use crate::config::*;
use crate::memory::partitionmemory::PartitionMemory;
use crate::store::Store;

pub mod tuples;
pub mod collections;
pub mod betamemory;
pub mod segmentmemory;
pub mod pathmemory;
pub mod partitionmemory;
pub mod tuplememory;

pub type BetaMemoryId = ArenaIndex;
pub type SegmentMemoryId = ArenaIndex;
pub type PathMemoryId = ArenaIndex;
pub type PartitionMemoryId = ArenaIndex;

/// The dynamic part of the phreak network, containing the asserted facts and
/// partial matches.
///
/// The memory is divided into partitions, so that rules in one partition can be processed
/// independent of an other partition.
pub struct KnowledgeSession {
    /// facts may be used from different partitions
    pub fact_store: Store,
    /// Network partitions that can be processed independent
    pub partition_memories: Arena<Mutex<PartitionMemory>>,
}

impl KnowledgeSession {
    pub fn new() -> Self {
        KnowledgeSession {
            fact_store: Store::with_capacity(START_STORE_SIZE),
            partition_memories: Arena::with_capacity(START_PARTITIONS_SIZE),
        }
    }
}

impl Default for KnowledgeSession {
    fn default() -> Self {
        Self::new()
    }
}
