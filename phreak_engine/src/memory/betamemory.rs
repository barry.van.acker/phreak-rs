use serde::{Deserialize, Serialize};

use crate::memory::collections::tuplesets::TupleSets;
use crate::memory::partitionmemory::RightTupleId;
use crate::memory::tuplememory::{LeftTupleMemoryEnum, RightTupleMemoryEnum};
use crate::memory::tuples::RightTuple;
use crate::network::nodes::PhreakBetaNodeType;

/// Memory for a single node in the beta network
#[derive(Debug, Deserialize, Serialize)]
pub struct BetaMemory {
    pub(crate) staged_right_tuples: TupleSets<RightTuple, RightTupleId>,
    pub(crate) left_tuple_memory: LeftTupleMemoryEnum,
    pub(crate) right_tuple_memory: RightTupleMemoryEnum,
    pub(crate) node_type: PhreakBetaNodeType,
}

impl BetaMemory {
    pub(crate) fn new(ltm: LeftTupleMemoryEnum,
                      rtm: RightTupleMemoryEnum,
                      node_type: PhreakBetaNodeType,
    ) -> Self {
        BetaMemory {
            staged_right_tuples: TupleSets::new(),
            left_tuple_memory: ltm,
            right_tuple_memory: rtm,
            node_type,
        }
    }

    pub fn get_staged_right_tuples(&self) -> &TupleSets<RightTuple, RightTupleId> {
        &self.staged_right_tuples
    }

    pub fn get_staged_right_tuples_mut(&mut self) -> &mut TupleSets<RightTuple, RightTupleId> {
        &mut self.staged_right_tuples
    }
}

