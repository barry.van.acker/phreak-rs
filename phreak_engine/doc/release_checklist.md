# Checklist for releases

0) Merge changes into release/version_number
1) Update CHANGELOG.md
2) Update cargo.toml lib versions
3) Update cargo.toml dependency versions
4) Merge release/version_number back to develop
5) Create merge request from release/version_number to master
6) Check and approve
7) Tag the master branch
 
## Important

Validate the pipeline after each push
