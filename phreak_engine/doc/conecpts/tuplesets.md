# Tuplesets

Tuplesets are used to gather the changes that are propagated between
two consecutive nodes in the network.

Tuplesets comes in two mayor types: LeftTuplesets and RightTuplesets.

Tuplesets contains three main sets: inserts, updates and deletes.

