# Rule Evaluation in Phreak

Phreak rule evaluation, just like ReteOO is object oriented, meaning that we will not
split the attributes of an object into individual facts, but instead we keep the
relation between object and attributes intact (and thus the relation between the 
attributes of a single object is also kept). This is a major improvement over traditional 
Rete, which needs to do a lot of correlations between attributes in order to determine
in-object relations.

Rule evaluation in the phreak algorithm is done in two fases. The alpha network eagerly 
processes new facts as they come in. Each fact object is processed as a complete entity 
by the alpha network, and all static tests are executed to filter the relevant fact objects.
The subsequent alphanodes keep filtering the stream of objects until only the objects that
are relevant remain.

When a fact reaches the end of the alpha network, it is registered in one of the tuplesets
of the betanetwork, depending on the connection between the networks. Both the alpha and 
the betanetwork are independent trees. LIA (left input adapter) nodes provide a link between 
the alpha and betanetwork by which new LeftToken trees are started. When a fact object reaches 
the LIA node, a new root LeftTuple is created and inserted in the segment tupleset for the 
corresponding betanode.

## Right tuples  

If a fact reaches a RightBridge node in the alpha network, a new RightTuple is generated, 
and inserted into the tupleset of the corresponding betanodes. (This node doesn't exist 
in Drools, but it allows us a more idiomatic approach in rust) A RightTuple indicates a
candidate fact object for matching.

## Left tuples

The root LeftToken indicates the start of a sequence of condition. Every subsequent 
LeftToken in the tree is a condition that is being met by the objects in the tuples. When
all conditions are met, the path from root to tip corresponds to all objects required to
fire the rule.

The betanetwork's job is to take the LeftTuples, and to apply the combination methods to find
a match for the rule. For example the JoinNode wil apply a join method: It will take a 
lefttuple, and tries to match this against all righttuples. Each valid match results in a 
child lefttoken, which is then propagated to the next betanode.

