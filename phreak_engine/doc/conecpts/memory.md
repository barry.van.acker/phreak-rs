# Memory

## Betamemory

Betamemory is used to store the tuples in the current node.

The betamemory has two main sets: RightTuples and LeftTuples, depending on the
incoming branch.

## SegmentMemory

A Segment in the network is a section of consecutive nodes, without branches. It
has a root and a tip node for start and end of the segment.

The segment has a reference to each Betamemory associated to the nodes in the 
segment, and no other segment has a reference to these.

The segment holds a list of staged LeftTuples, that are staged for the rootNode
of the segment.

A segment has a collection of backreferences to the pathmemories to which it
belongs.

The segment has various masks, indicating readiness states.

## PathMemory

A Path in the network is a collection of consecutive segments that lead from
a segment with an entrypointNode as the rootNode upto a segment with a 
terminalNode for tipNode. Segments may occur in multiple Paths

A path belongs to a Rule Agenda Item. It represents what is needed to activate
a certain rule.

## PartitionMemory

A Partition is a part of the Network that contains only nodes that are
connected with each other: every node in the partition has a path to the
other nodes in the partition, and no two nodes in different partitions are
connected.

A PartitionMemory is assigned to the partition to store it's data. It 
contains all tuples that are used in the partition, and all memories 
mentioned above (Node, Segment and PathMemories).

Partitions will be the unit of parallel processing.
