# Agenda

The agenda is the owner of all the individual rules of the Phreak Network. Each 
rule has a priority, and the agenda organizes and schedules the individual rules for
execution.

Each AgentaRule owns a PathMemory and a RuleExecutor.
