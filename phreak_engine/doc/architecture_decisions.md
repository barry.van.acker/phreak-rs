# Architecture Decision Records

* [ADR-000: Documenting Architecture Decisions](adr/adr-000.md)
    
    All architecture decisions are documented according to the format described on the blog
    [Documenting Architecture Decisions](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions)
    
* [ADR-001: Safe Rust](adr/adr-001.md)

    We will not use unsafe rust in our own code.
    
* [ADR-002: Async ready](adr/adr-002.md)

    We will use partitioning to allow Async in the future. The 
    compartmentalization of the network will allow parallel processing by
    executing each partition update as a task in a multi executor setup.
    
