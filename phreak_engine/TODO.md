# TODO LIST

## Rule Concepts
1. Rule removing
1. Query nodes
1. Agenda and Scheduling
1. Data Types (we have strings, we need integers, floats, dates, time, datetime)
1. Recalculate state after adding rules to a network that already contains facts

## Implementation details
1. Replace TreeArena by a GenerationalArena backed implementation, as the current TreeArena does 
support deletes, but doesn't re-use the vacant spots.
1. Stack based processing (basics are added)
1. Unlinking segments
1. Unlinking rules
1. Modified segments
1. BTree based token lists and scheduled actions.

## Other functionality
1. Snapshot to disk and back
1. Parallel processing where possible and real improvement

# Test matrix

This matrix shows the combinations of nodes we need to test. The left side shows the parent nodes,
the top side shows the child nodes. When a combination of a parent and a child is feasible, we need
to test it.

|  | root | join | memory | production | negative | ncc | ncp|
|---|---|---|---|---|---|---|---|
|root|-|2|-|1|4|7|-|
|join|-|-|3|2|-|-|7|
|memory|-|3|-|-|5|?|-|
|production|-|-|-|-|-|-|-|
|negative|-|5|-|4|6|9|10|
|ncc|-|8|-|7|11|8|8|
|ncp|-|-|-|-|-|-|-|

T = Todo <br>
1 = Present in testcase nr 1
