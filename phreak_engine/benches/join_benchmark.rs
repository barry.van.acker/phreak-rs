use criterion::{Criterion, criterion_group, criterion_main};

use phreak_engine::config::GeneralConfig;
use phreak_engine::memory::KnowledgeSession;
use phreak_engine::network::conditions::JoinConstraint;
use phreak_engine::prelude::*;
use phreak_facts::{FactsBuilder, Value};

fn helper_create_fact_car(store: &mut Store) -> FactHandle {
    let fact_car = FactsBuilder::new("Car".to_owned())
        .add_attributes("color".to_string(), "red".to_string())
        .add_attributes("type".to_string(), "hatchback".to_string())
        .add_attributes("brand".to_string(), "Toyota".to_string())
        .build();
    store.add_fact(fact_car)
}

fn helper_create_fact_tree(store: &mut Store) -> FactHandle {
    let fact_tree = FactsBuilder::new("Tree".to_owned())
        .add_attributes("kind".to_string(), "Maple".to_string())
        .add_attributes("genus".to_string(), "Acer".to_string())
        .add_attributes("color".to_string(), "red".to_string())
        .build();

    store.add_fact(fact_tree)
}

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut network = Network::new();
    let mut ks = KnowledgeSession::new();
    let config = GeneralConfig::default();

    let mut on_id = None;
    let mut an_id = None;
    let (start_id, end_id) = AlphaNetworkBuilder::new(&mut network)
        .create_entrypoint_node()
        .get_or_create_objecttype_node("Car", &mut on_id)
        .get_or_create_alpha_node(Condition {
            field: Value::from("color"),
            operator: Operator::Equal,
            expr: Value::from("red"),
        }, &mut an_id)
        .collect()
        ;

    let mut lian_id = None;
    let mut jn_id = None;
    let mut pm_id = None;

    let pid = BetaNetworkBuilder::new(&mut network, &mut ks, &config)
        .start_partition(&mut pm_id)
        .start_path(start_id)
        .start_segment(pm_id.unwrap())
        .get_or_create_lia_node(on_id.unwrap(), &mut lian_id)
        .get_or_create_betajoin_node(an_id.unwrap(), &mut jn_id, pm_id.unwrap(),
                                     JoinConstraint::ByHandle)
        .collect(start_id, end_id);


    c.bench_function("red colored cars",
                     |b| {
                         let mut count = 0;
                         b.iter(|| {
                             let h1 = helper_create_fact_car(&mut ks.fact_store);
                             let h2 = helper_create_fact_tree(&mut ks.fact_store);

                             {
                                 let pm = &mut ks.partition_memories[pm_id.unwrap()].lock().unwrap();
                                 let partition = &network.partitions[pid];

                                 partition.assert_object(h1, &network, &mut ks.fact_store, pm);
                                 partition.assert_object(h2, &network, &mut ks.fact_store, pm);
                             }
                             // increasing the ITERATION_COUNT in config.rs corresponds to this count
                             // larger batches are faster per assert
                             if count >= 1024 {
                                 evaluate(&network, &ks);
                                 count = 0;
                             } else {
                                 count += 1;
                             }
                         });
                     });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);