Support package for phreak_engine.

The rules crate contains the abstraction for the phreak input. A rule is a collection of
conditions that must be met in order to activate.
