//! Support package for phreak_engine.
//!
//! The rules package encapsules the rules, so that way can separate our logic into the
//! creation and the usage of rules.
use std::fmt::Debug;

/// A rule is the starting point
#[derive(Debug)]
pub struct Rule {
    pub name: String,
    pub classes: Vec<Class>,
    pub production: Box<dyn ProductionCallback>
}

/// A class is a way to group facts and usually corresponds to a struct in your program data
#[derive(Debug)]
pub struct Class {
    pub name: String,
    pub alias: String,
    pub conditions: Vec<Condition>,
}

/// A condition is something to test for
#[derive(Debug)]
pub struct Condition {
    pub field: String,
    pub operator: Operator,
    pub expr: String,
}

/// An operator defines a testing directive
#[derive(Debug)]
pub enum Operator {
    Equal,
    NotEqual,
    LessThan,
    LessEqual,
    MoreThan,
    MoreEqual
}

pub trait ProductionCallback : Debug {
    fn activate(& self);
    fn deactivate(& self);
}

#[derive(Debug)]
pub struct PrintCallBack {}

impl ProductionCallback for PrintCallBack {
    fn activate(& self) {
        println!("1");
    }

    fn deactivate(& self) {
        println!("1");
    }
}


impl Default for Box<dyn ProductionCallback> {
    fn default() -> Self {
        Box::new(PrintCallBack{} )
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_rule_tree() {

        let mut class1 = Class{
            name: "Class 1".to_string(),
            alias: "Alias 1".to_string(),
            conditions: vec![]
        };

        class1.conditions.push(Condition{
            field: "condition1".to_string(),
            operator: Operator::Equal,
            expr: "expr1".to_string()
        });

        class1.conditions.push(Condition{
            field: "condition2".to_string(),
            operator: Operator::LessEqual,
            expr: "expr2".to_string()
        });

        //
        let mut class2 = Class{
            name: "Class 2".to_string(),
            alias: "Alias 2".to_string(),
            conditions: vec![]
        };

        class2.conditions.push(Condition{
            field: "condition3".to_string(),
            operator: Operator::Equal,
            expr: "expr3".to_string()
        });

        class2.conditions.push(Condition{
            field: "condition4".to_string(),
            operator: Operator::LessEqual,
            expr: "expr4".to_string()
        });

    }
}
