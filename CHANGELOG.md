# Changelog

## [Unreleased]

## [0.1.5] - 2020-05-11

### Added
- Skipping unfeasible segments in the graph
- Introduce data types
- Add comparisions in Alphanodes and Betanodes

## [0.1.4] - 2020-04-21

### Added
- Rule addition
- Ordering on tokens, so that delayed actions can be resolved against the correct tokenset
- Ordering on facts
- Lifespans on memory items
 
## [0.1.3] - 2020-04-05

### Added
- Multiple Negative Conjunct Matching, for both adding and removing facts

### Fixed
- Replaced indextree by generational-indextree, with better support for removing nodes.

## [0.1.2] - 2020-02-23

### Added
- Negative Single Conjunct matching, both adding and removing facts

### Fixed
- Recalculate initial state after adding a rule

## [0.1.1] - 2020-02-10

### Added
- Fact Removal

## [0.1.0] - 2020-02-07

### Added

- Positive Conjunct matching
- Fact Addition
