# Phreak

[![dependency status](https://deps.rs/repo/gitlab/barry.van.acker/phreak-rs/status.svg)](https://deps.rs/repo/gitlab/barry.van.acker/phreak-rs)
[![License GPL3+](https://img.shields.io/crates/l/phreak_engine.svg)](https://gitlab.com/barry.van.acker/phreak-rs/-/blob/develop/phreak_engine/LICENSE.md)
[![doc.rs](https://docs.rs/phreak_engine/badge.svg)](https://docs.rs/phreak_engine)

Implementation of the phreak algorithm in Rust

The Phreak algorithm is a Rule matching algorithm, based upon the Rete algorithm, as described
in the paper "Production Matching for Large Learning Systems" by Robert B. Doorenbos,
January 31, 1995, CMU-CS-95-113

The algorithm is used to match facts against a number of rules to see if any combination
triggers the rule. A rule is called a production, and when a combination of facts matches the
rule, it is called an activation.

The phreak algorithm is implemented by the Drools project. We are not trying to create a java
to rust port, but instead we are building an algorithm from scratch with the ideas from the
Drools documentation.

We aim to use only safe rust code.

We are trying to obtain a good performance by focusing on the following concepts:
 
 - Lazy evaluation
 
   The fasted code is code you don't execute.
 
 - Cache friendly evaluation

   By grouping data evaluation, we try to optimize cache usage for the CPU.
 
 - Minimize data allocations
 
   Allocations are slow, so they should be avoided. We use the rust ownership model to safely
   pass around data instead of passing on copies.

# Project structure

The main package for this repository is phreak_engine. It contains the code for the network, and performs
the heavy lifting.

The phreak_facts and phreak_rules packages wrap the input types for the network.

The phreak_parser package will contain a rule parse at some time, but it is empty now. The parser will
translate human written rules into the rule structures used by the phreak network.
